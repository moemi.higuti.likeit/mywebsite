/*---------------*navbar */
$(document).ready(function(){
    $('.menu-toggle').click(function(){
        $('nav').toggleClass('active');
    });
    $('ul li').click(function(){
        $(this).siblings().removeClass('active');
        $(this).toggleClass('active');
    });
});




/*----------------slider--------*/ 
$(function() {
     $('.slider').slick({
          autoplay: true,
          centerMode: true,
          centerPadding: '35%',
          slideToShow: 1,
          focusOnSelect: true,
          speed: 1000,
          responsive: [
               {
                breakpoint: 768,
                settings: {
                    centerMode: false
                }
            }
        ]
    });
});

/**---------------login password--------------- */

function show(){
    var pswrd = document.getElementById('pswrd');
    var icon = document.querySelector('.fa-eye');
    if(pswrd.type === "password") {
        pswrd.type = "text";
        icon.style.color = "#1da1f2";
    }else{
        pswrd.type = "password";
        icon.style.color = "black";
    }
}
/**-----------------ログインフォームを出す--------------------- */
$(function(){
    $('.login').click(function(){
      $('#form').fadeIn();
    });
    $('#close').click(function(){
      $('#form').fadeOut();
    });
  })


/**商品　紹介の最大三枚までのギャラリー　 */
  function imageGallery() {
    const highlight = document.querySelector(".gallery-hightlight");
    const previews = document.querySelectorAll(".room-preview img");
  
    previews.forEach(preview => {
      preview.addEventListener("click", function() {
        const smallSrc = this.src;
        const bigSrc = smallSrc.replace("small", "big");
        previews.forEach(preview => preview.classList.remove("room-active"));
        highlight.src = bigSrc;
        preview.classList.add("room-active");
      });
    });
  }
  
  imageGallery();


  /**商品登録三枚の画像 */
  function showPreview(event){
    if(event.target.files.length > 0 ){
      var src = URL.createObjectURL(event.target.files[0]);
      var preview = document.getElementById("file-ip-1-preview");
      var previewbig = document.getElementById("file-ip-big-preview");

      previewbig.src = src;
      preview.src = src;
      preview.style.display = "block";
      previewbig.style.display = "block";
    }
  }

  function showPreview2(event){
    if(event.target.files.length > 0 ){
      var src = URL.createObjectURL(event.target.files[0]);
      var preview = document.getElementById("file-ip-2-preview");

      preview.src = src;
      preview.style.display = "block";
    }
  }

  function showPreview3(event){
    if(event.target.files.length > 0 ){
      var src = URL.createObjectURL(event.target.files[0]);
      var preview = document.getElementById("file-ip-3-preview");

      preview.src = src;
      preview.style.display = "block";
    }
  }


  function show(){
    var pswrd = document.getElementById('pswrd');
    var icon = document.querySelector('.fa-eye');
    if(pswrd.type === "password") {
        pswrd.type = "text";
        icon.style.color = "#1da1f2";
    }else{
        pswrd.type = "password";
        icon.style.color = "black";
    }
}

function showpassword(){
    var inputPassword = document.getElementById('inputPassword');
    var eye = document.getElementById('eye');
    if(inputPassword.type === "password") {
        inputPassword.type = "text";
        eye.style.color = "#1da1f2";
    }else{
        inputPassword.type = "password";
        eye.style.color = "black";
    }
}
