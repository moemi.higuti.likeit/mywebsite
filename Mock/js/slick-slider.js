
$(function() {
    $('.thumb-item').slick({
         infinite: true,
         slidesToShow: 1,
         slidesToScroll: 1,

         arrows: false,
         fade: true,
         asNavFor: '.thumb-item-nav' //サムネイルのクラス名
    });
    $('.thumb-item-nav').slick({
         infinite: true,
         prevArrow: '<div class="slider-arrow slider-prev fa fa-angle-left"></div>',
     nextArrow: '<div class="slider-arrow slider-next fa fa-angle-right"></div>',

         slidesToShow: 4,
         slidesToScroll: 1,
         asNavFor: '.thumb-item', //スライダー本体のクラス名
         focusOnSelect: true,
    });
});