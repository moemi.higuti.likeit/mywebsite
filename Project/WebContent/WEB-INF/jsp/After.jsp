<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>お買い求め確認</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/user_buy_confirm.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">


    </head>
    <body id=body>

        <header>
            <a href="#" class="logo">Hi.</a>
                <nav>
                    <ul>
                        <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                            <ul>
                                <li><a href="#">陶器</a></li>
                                <li><a href="#">折り紙</a></li>
                                <li><a href="#">手芸</a></li>
                                <li><a href="#">手作りマスク</a></li>
                                <li><a href="#">アクセサリー</a></li>
                                <li><a href="#">小物</a></li>
                            </ul>
                        </li>
                        <li id="login"><a class="login" href="#" >ログイン</a></li>
                        <li><a href="#">新規登録</a></li>
                        <li><a class="active" href="#">カート</a></li>
                    </ul>
                </nav>
                <div class="menu-toggle"><i class="fas fa-bars"></i></div>
        </header>

        <div class="buscar-caja">
            <input type="text" name="" class="buscar-txt" placeholder="検索">
            <a class="buscar-btn">
            <i class="fas fa-search"></i>
            </a>
          </div>


    <!---------------------------ログイン------------------------>

        <div class="form" id="form">
          <div class="loginform">
            <div class="header">Login Form
                <i id="close" aria-hidden="false" class="far fa-times-circle"></i></div>

            <form action="">
                <div class="input-field">
                    <input type="text" required>
                    <label>login Id</label>
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                </div>
                <div class="input-field">
                    <input id="pswrd" type="password" required>
                    <label>Password</label>
                    <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
                </div>
                <div>
                        <button type="submit" class="btn-stitch">Login</button>
                    </div>
                </form>
                <div class="new">
                    <a href="#">新規登録</a>
                </div>
            </div>
        </div>
    <!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

    <main>
        <section>
            <h2 class="my-5 buy_confirm_title">${user.login_id} さん カートの中身</h2>
		<input type="hidden" name="id" value="${user.id}">
              <table class="table_buy">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">作品名</th>
                    <th scope="col">販売価格</th>
                    <th scope="col">数量</th>
                    <th scope="col">小計 <small>税込み</small></th>
                    <th scope="col">削除</th>
                  </tr>
                </thead>

                <!-- ここから挿入していくよ -->

			<form action="ItemDelete" method="POST" name="form1">

                <tbody>
                <c:forEach var="item" items="${cart}" varStatus="status">

                  <tr>
                    <th scope="row">
                        <div class="item_title">
                            <img class="confirm_img" src="img/${item.file_name1}" alt="">
								<a href="ItemDetail?item_id=${item.id}"><p>${item.item_name}</p></a>
                        </div>
                    </th>

                    <td id="SinglePrice">${item.price}</td>
                    <td>

                    <!-- アイテムの残りの量とほしい数量の選択ーーーーーーーーーーーーーーーー -->
                        <select name="itemNumber" onChange="keisan()">
							<c:forEach var="item" items="${cart}" begin="1" end="${item.number}" varStatus="status">
								 <option value="${item.number}">${item.number}</option>
							</c:forEach>
                        </select>

                    </td>
                    </c:forEach>
                    <!-- 数量と小計の合計額を入れるーーーーーーーーーーーーーーーー -->

                    <td><input type="text" name="field1" size="8" value="0"> 円</td>

                    <td><button class="btn-stitch">削除</button></td>
                  </tr>

				</form>


                </tbody>

                  <tbody>
                    <tr>
                      <th scope="row">
                      </th>
                      <!-- 配送方法の情報の取得と入力 -->
                      <td class="delivary_title">配送方法</td>
                      <td>
                          <select name="delivery_method_id">
								<c:forEach var="dmdb" items="${dmdbList}" >
									<option value="${dmdb.id}">${dmdb.name}</option>
								</c:forEach>
						  </select>
                      </td>

                      <c:choose>

                      <c:when test = "${dmdb.id == 1}">
						<td>500 <small>円</small></td>
			          </c:when>
			          <c:when test = "${dmdb.id == 2}">
						<td>無料</td>
			          </c:when>
			          <c:when test = "${dmdb.id == 3}">
						<td>200 <small>円</small></td>
			          </c:when>

                      </c:choose>

                    </tr>
                    </tbody>
              </table>
        </section>
        <section>
            <div class="total">
                <h3 class="buy_confirm_title">お買い求め金額</h3>
                <h3>10000000000 円</h3>
            </div>
        </section>
        <section>
            <div class="buy-button row"></div>
                <button class="btn-stitch">購入</button>
                <button class="btn-back">戻る</button>

        </section>

    </main>

<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript">


function keisan(){

	// 設定開始
	var singleprice = document.getElementById('SinglePrice');

	// 商品1
	var price1 = document.form1.goods1.selectedIndex * singleprice; // 単価を設定
	document.form1.field1.value = price1; // 小計を表示

	// 商品2
	var price2 = document.form1.goods2.selectedIndex * 1000; // 単価を設定
	document.form1.field2.value = price2; // 小計を表示

	// 商品3
	var price3 = document.form1.goods3.selectedIndex * 3000; // 単価を設定
	document.form1.field3.value = price3; // 小計を表示

	// 商品4
	var price1 = document.form1.goods1.selectedIndex * 500; // 単価を設定
	document.form1.field1.value = price1; // 小計を表示

	// 商品5
	var price2 = document.form1.goods2.selectedIndex * 1000; // 単価を設定
	document.form1.field2.value = price2; // 小計を表示

	// 商品6
	var price3 = document.form1.goods3.selectedIndex * 3000; // 単価を設定
	document.form1.field3.value = price3; // 小計を表示

	// 商品7
	var price1 = document.form1.goods1.selectedIndex * 500; // 単価を設定
	document.form1.field1.value = price1; // 小計を表示

	// 商品8
	var price2 = document.form1.goods2.selectedIndex * 1000; // 単価を設定
	document.form1.field2.value = price2; // 小計を表示

	// 商品9
	var price3 = document.form1.goods3.selectedIndex * 3000; // 単価を設定
	document.form1.field3.value = price3; // 小計を表示



	// 設定終了


	document.form1.field_total.value = total; // 合計を表示

}


</body>
</html>
