<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>お買い求め確認</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/user_buy_confirm.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">
        <Style>
			.wantNm{
				;
			}
        </Style>


      <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

			<!-- -------------検索バー---------- -->
			    <form action="ItemSearchResult">
			    	<div class="buscar-caja">
				        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
				        <a class="buscar-btn">
				        	<i class="fas fa-search"></i>
				        </a>
			      </div>
				</form>

<!-- ーーーーーーーーー    上記までは  user html 共有する      ーーーーーーー-->



    <main>
        <section>
            <h2 class="my-5 buy_confirm_title">${user.login_id} さん カートの中身</h2>
            <div>${cartActionMessage}</div>

		<input type="hidden" name="id" value="${user.id}">
              <table class="table_buy">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">作品名</th>
                    <th scope="col">販売価格　/　購入個数</th>
                    <th scope="col">小計 <small>税込み</small></th>
                    <th scope="col">アンケート</th>
                  </tr>
                </thead>

                <!-- ここから挿入していくよ -->
                <c:forEach var="item" items="${item}" >

	                <tbody>
	                  <tr>
	                    <th scope="row">
	                        <div class="item_title">
	                            <img class="confirm_img" src="img/${item.file_name1}" alt="">
									<a href="ItemBuyDetail?item_id=${item.id}"><p>${item.item_name}</p></a>
	                        </div>
	                    </th>
	                    <td>${item.formatPrice}<small>円</small>　/　${item.buyNumber} <small>点</small> </td>
	                    <td>${item.formatTotalPrice} 円</td>
						<td><a href="UserBuiedDetail?item_id=${item.id}"><p><img class="confirm_img" src="img/${item.file_name1}" alt=""></p></a></td>
	                   </tr>
				</c:forEach>

                </tbody>

                  <tbody>
                    <tr>
                      <th scope="row">
                      </th>
                      <!-- 配送方法の情報の取得と入力 -->

                      <td class="delivary_title"> ${bdb.deliveryMethodName}</td>
                      <td>
                          ${bdb.deliveryMethodPrice}円</td>
                      </td>
                    </tr>
                    </tbody>
              </table>
        </section>

        <section>
            <div class="total">
                <h3 class="buy_confirm_title">お買い求め金額</h3>
                <h3>${total} <small>円</small></h3>
            </div>
        </section>

        <section>

                <a href="User"  class="btn-back">ログイン画面に戻る</a>

        </section>

    </main>

<script type="text/javascript" src="js/index.js"></script>



</body>
</html>
