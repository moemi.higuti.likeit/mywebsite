<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>トップページ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">

</head>
<body id=body>

    <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>
                     <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>



    <!-- -------------検索バー---------- -->
    <form action="ItemSearchResult">
    	<div class="buscar-caja">
	        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
	        <a class="buscar-btn">
	        	<i class="fas fa-search"></i>
	        </a>
      </div>
	</form>

<!---------------------------ログイン------------------------>

    <div class="form" id="form">
    <form   action="Login" method="POST">
      <div class="loginform">
        <div class="header">Login Form
            <i id="close" aria-hidden="false" class="far fa-times-circle"></i></div>


            <div class="input-field">
                <input name="loginId" type="text" required>
                <label>login Id</label>
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </div>
            <div class="input-field">
                <input name="password" id="pswrd" type="password" required>
                <label>Password</label>
                <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <div>
                    <button type="submit" class="btn-stitch">Log in</button>
                </div>
            </form>
            <div class="new">
                <a href="SingUp">新規登録</a>
            </div>
        </div>

    </div>
<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

	<c:if test="${errMsg != null}">
    <section>
		${errMsg}
		<br>
	 </section>
	</c:if>

    <div class="container slick">
        <div class="title">
            <h2>News</h2>

        </div>

        <ul class="slider">
            <li><a href="SoldOut"><img src="img/soldout.jpg">
                <p>売り切れ商品一覧ページ追加!!</p>
                </a></li>
            <li><a href="User"><p>購入者限定アンケート調査開始</p>
                <img src="img/akuse.jpg">
                </a></li>
            <li><a href="User"><img src="img/doll.jpg">
                <p>購入者の詳細ページ追加</p>
                </a></li>
            <li><a href="User"><p>販売者の詳細ページ追加</p>
                <img src="img/eggs.jpg">
                </a></li>
            <li><a href="User"><img src="img/handmade (2).jpg">
                <p>アンケートがみられる機能追加!!!</p></a></li>
            <li><a href="User"><p>まだまだ追加するかも？</p>
                <img src="img/money.jpg"></a></li>
        </ul>
    </div>

    <main>






        <!-----------サイト紹介------------>

        <div class="container">
            <div class="introduce">
                <div class="card mb-12" style="max-width: 100%;">
                    <div class="row no-gutters">
                        <div class="row content">
                            <div class="intro_img">
                            <!----------イメージ集-->
                                <div class="bannrerImg" id="slideshow">
                                    <img src="img/mishin.jpg"  class="active">
                                    <img src="img/haisou.jpg"  alt="">
                                    <img src="img/teruteru.jpg"  alt="">
                                </div>
                            </div>
                            <div class="intro_text">
                                <div class="bannerText" id="slideshowText">

                                    <div class="text active">
                                        <h5 class="card-title">初めまして　Hi.　です</h5>
                                        <p class="card-text">作家さんから作品を購入</p>
                                        <p class="card-text"><small class="text-muted">
                                            作品を「買いたい人」と「売りたい人」をつなぐサービスです。<br>
                                            作家さんから作品を直接購入することができます。</small></p>
                                    </div>
                    <!-------2ND CONTENTS--------->
                                <div class="text">
                                    <h5 class="card-title">Hi.のある生活</h5>
                                    <p class="card-text">「商品」ではなく「作品」</p>
                                    <p class="card-text"><small class="text-muted">販売しているのは「商品」ではなく「作品」です。<br>
                                        1000万点の作品との新しい出会いをお届けします。</small></p>
                                </div>
                        <!-------3RD CONTENTS--------->
                                <div class="text">
                                    <h5 class="card-title">あなたもHi.をはじめてみませんか？</h5>
                                    <p class="card-text">安心、安全な取引</p>
                                    <p class="card-text"><small class="text-muted">購入・販売の際のお金のやりとりは弊社が仲介するので安全です。<br>
                                        どうぞ安心してお買い物をお楽しみください。</small></p>
                                </div>

                                <ul class="controls">

                                    <li><a><img src="img/next.png" onclick="prevSlide();prevSlideText();"></a></li>
                                    <li><a><img src="img/prev.png" onclick="nextSlide();nextSlideText();"></a></li>
                                </ul>

                            </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!------おすすめカテゴリー--------------->
        <section>
            <div class="container">
                <div class="select">
                    <div class="title">
                        <h2 class="item-title"><small>おすすめカテゴリー</small></h2>

                    </div>
                    <div class="cards">

							<c:forEach var="cate" items="${categoryList}">

	                            <div class="singleBlog">
	                                <img src="img/${cate.category_img}" alt="">
	                                <div class="blogCotent">
	                                    <h3>${cate.category_name}</h3>
	                                    <a href="Category?categoryName=${cate.category_name}" class="btn-stitch">詳細</a>
	                                </div>
	                            </div>
                            </c:forEach>

                    </div>


                </div>
            </div>
        </section>

        <!---------おすすめ紹介---------------->
        <section>
            <div class="container">

                    <div class="title">
                        <h2 class="item-title">Hi.<small>のおすすめ</small></h2>
                        <p>スタッフが心をつかまれた作品を紹介</p>
                    </div>

                        <div class="rec_cards row">

                            <!------first stard------>
                            <c:forEach var="item" items="${itemList}">
	                            <div class="item-rec-card x-3 col-md-3">
	                               <div class="single-rec-card">
	                                <div class="product-top">
	                                    <img src="img/${item.file_name1}" alt="">
	                                    <div class="overlay">
	                                        <button type="button" class="btn-btn-secondary" onclick="toggle()" title="Add to Favorite">
	                                            <i class="fa fa-heart" id="o" aria-hidden="true"></i>
	                                        </button>
	                                    </div>
	                                </div>
	                                 <a href="ItemDetail?item_id=${item.id}">

	                                        <p class="mt-3">${item.formatPrice} <small>円</small></p>
	                                        <p>${item.item_name}</p>
	                                        </a>
	                                       <a href="ItemDetail?item_id=${item.id}" class="btn-stitch">詳細</a>

	                           	 </div>
	                            </div>
                            </c:forEach>
              <!----------- done card--------->
                        </div>
                    </div>

    </section>
    </main>
    <footer>
        <div class="container footer-row">
            <hr>
            <div class="footer-left-col">
                <div class="footer-links">
                    <div class="mb-3 link-title">
                        <h4>Hiを知る</h4>
                        <small>Hiについて</small><br>
                        <small>Hiとつながる</small>
                    </div>
                    <div class="mb-3 link-title">
                        <h4>作品販売について</h4>
                        <small>掲載について</small><br>
                        <small>作品販売の規約</small>
                    </div>
                    <div class="link-title">
                        <h4>ヘルプとガイド</h4>
                        <small>ヘルプ</small><br>
                        <small>ガイド</small>
                    </div>
                </div>
            </div>
            /<hr>
        </div>
    </footer>


    <script type="text/javascript" src="js/index.js"></script>

    <script>
        //お気に入りの色の切り替え
        function toggle(){
            const o = document.getElementById('o');
            if( o.style.color = "#000"){
               // o.innerHTML="<i class="fa fa-heart changed" aria-hidden="true"></i>"
               o.style.color = "red";
            }else if(o.style.color = "red"){
                o.style.color = "#000"
            }
        }




        //サイト紹介の切り替え
                var slideshow = document.getElementById('slideshow');
            var slides = slideshow.getElementsByTagName('img');
            var index = 0;

            function nextSlide(){
                slides[index].classList.remove('active');
                index = (index + 1) % slides.length;
                slides[index].classList.add('active');
            }
            function prevSlide(){
                slides[index].classList.remove('active');
                index = (index - 1 + slides.length) % slides.length;
                slides[index].classList.add('active');
            }


            var slideshowText = document.getElementById('slideshowText');
            var slidesText = slideshowText.getElementsByTagName('div');
            var i = 0;

            function nextSlideText(){
                slidesText[i].classList.remove('active');
                i = (i + 1) % slidesText.length;
                slidesText[i].classList.add('active');
            }
            function prevSlideText(){
                slidesText[i].classList.remove('active');
                i = (i - 1 + slidesText.length) % slidesText.length;
                slidesText[i].classList.add('active');
            }
            function menuToggle(){
                var nav = document.getElementById('navbar');
                nav.classList.toggle('active');
            }

            </script>
        </body>
        </html>
