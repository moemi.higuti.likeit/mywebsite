<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>商品結果一覧</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/item_serch_result.css">


</head>
<body id=body>

    <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
              <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>
                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

    <div class="buscar-caja">
        <input type="text" name="" class="buscar-txt" placeholder="検索">
        <a class="buscar-btn">
        <i class="fas fa-search"></i>
        </a>
      </div>


<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         --><header>
        <a href="#" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                            	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                                  <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                                  <li><a href="Category?categoryName=手芸">手芸</a></li>
                                  <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                                  <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                                  <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>
                    <c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		                <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
                    <li><a href="Cart">カート</a></li>
                    </c:if>
                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>



    <!-- -------------検索バー---------- -->
    <form action="ItemSearchResult">
    	<div class="buscar-caja">
	        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
	        <a class="buscar-btn">
	        	<i class="fas fa-search"></i>
	        </a>
      </div>
	</form>

<!---------------------------ログイン------------------------>

    <div class="form" id="form">
    <form   action="Login" method="POST">
      <div class="loginform">
        <div class="header">Login Form
            <i id="close" aria-hidden="false" class="far fa-times-circle"></i></div>


            <div class="input-field">
                <input name="loginId" type="text" required>
                <label>login Id</label>
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </div>
            <div class="input-field">
                <input name="password" id="pswrd" type="password" required>
                <label>Password</label>
                <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <div>
                    <button type="submit" class="btn-stitch">Log in</button>
                </div>
            </form>
            <div class="new">
                <a href="SingUp">新規登録</a>
            </div>
        </div>

    </div>
<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

<section>
    <div class="container">
        <div class="recomend">

            <div class="title">
                <h2 class="pb-3 item-title">検索結果一覧</h2>
                <p>
					検索結果${itemCount}件
				</p>
            </div>

            <div class="card-container my-5">
                <div class="rec_cards row">

					<c:forEach var="item" items="${itemList}" varStatus="status">
                    <!------first　card------>

                    <div class="item-rec-card x-3 col-md-3">
                        <div class="single-rec-card">
                         <div class="product-top">
                             <div class="overlay">
                                 <button type="button" class="mt-2 btn-btn-secondary" onclick="toggle()" title="Add to Favorite">
                                     <i class="fa fa-heart" id="o" aria-hidden="true"></i>
                                 </button>
                             </div>
                         </div>
                         <a href="Item?item_id=${item.id}&page_num=${pageNum}">
                         		<img src="img/${item.file_name1}">
                                 <p class="mt-3">${item.formatPrice}
                                 <small>円</small></p>
                                 <p>${item.item_name}</p>
                                 <a href="ItemDetail?item_id=${item.id}" class="btn-stitch">詳細</a>

                         </a>
                    	 </div>
                     </div>
					</c:forEach>
      			</div>
			</div>

      <!----------first row done--------->

       	<c:if test="${(status.index + 1) % 4 == 0}">

			<div class="rec_cards my-5 row"></div>
		</c:if>


		</div>

            <div class="row center">
                <ul class="pagination">


				<!-- １ページ戻るボタン  -->
                <c:choose>
					<c:when test="${pageNum == 1}">
						<li class="mx-3 disabled"><a href="ItemSearchResult?search_word=${searchWord}&page_num=${pageNum - 1}"><i class="fas fa-angle-left"></i></a></li>
					</c:when>
					<c:otherwise>
						<li class="mx-3 waves-effect"><a href="ItemSearchResult?search_word=${searchWord}&page_nuam=${pageNum - 1}">
						<i class="fas fa-angle-left"></i></a></li>
					</c:otherwise>
				</c:choose>


						<!-- ページインデックス -->
				<c:forEach begin="${(pageNum - 5) > 0 ? pageNum - 5 : 1}" end="${(pageNum + 5) > pageMax ? pageMax : pageNum + 5}" step="1" varStatus="status">
					<li <c:if test="${pageNum == status.index }"> class="active" </c:if>>
						<a class="mx-4" href="ItemSearchResult?search_word=${searchWord}&page_num=${status.index}">${status.index}</a>
					</li>
				</c:forEach>



					<!-- 1ページ送るボタン -->
					<c:choose>
						<c:when test="${pageNum == pageMax || pageMax == 0}">
							<li class="mx-3 disabled"><a><i class="fas fa-angle-right"></i></a></li>
						</c:when>
						<c:otherwise>
							<li class="mx-3 waves-effect"><a href="ItemSearchResult?search_word=${searchWord}&page_num=${pageNum + 1}">
							<a href="ItemSearchResult?search_word=${searchWord}&page_num=${pageNum + 1}"><i class="fas fa-angle-right"></i></a></li>
						</c:otherwise>
					</c:choose>

                </ul>
            </div>
           </div>
        <a href="Index" class="btn-back">トップページに戻る</a>
    </section>

    <script type="text/javascript" src="js/index.js"></script>
    </body>
    </html>