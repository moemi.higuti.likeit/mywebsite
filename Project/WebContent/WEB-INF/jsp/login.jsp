<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>ログイン</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/login.css">
        <link rel="stylesheet" href="css/button.css">
        <style>
        	.loginError{
        		padding: 10px;
        		font-size: 8px;
        		width:40%;
        		margin: 0 auto 0 auto;
        	}

        </style>


</head>
<body id=body>

    <header>
        <a href="Index" class="logo">Hi.</a>

    </header>





<!---------------------------ログイン------------------------>

    <div class="form" id="form">
      <div class="loginform">
        <div class="header">Login Form
            </div>

        <form action="LoginResult" method="POST">
            <div class="input-field">
                <input name="loginId" type="text" required>
                <label>login Id</label>
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </div>
            <div class="input-field">
                <input name="password" id="pswrd" type="password" required>
                <label>Password</label>
                <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <div class="button">
                <div class="inner">
                    <button type="submit" class="btn-stitch">Log in</button>
                </div>
                <input type="hidden" name="id" value="${user.id}">
            </form>
            <div class="new">
                <a href="#">新規登録</a>
            </div>
        </div>
    </div>


<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

    <c:if test="${loginErrorMessage != null || loginActionMessage != null}">
    <section class="loginError">
	    <div>
	    <h2>${loginErrorMessage}</h2>

		<h2>${loginActionMessage}</h2>
	     </div>
	</section>
	</c:if>

	<script type="text/javascript" src="js/index.js"></script>

	</body>
	</html>