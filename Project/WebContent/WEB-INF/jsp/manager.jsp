<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>ログイン画面</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/user.css">
        <link rel="stylesheet" href="css/user-his.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">
        <link rel="stylesheet" href="css/detail.css">
        <style type="text/css">

        	.buttons-handle {
				    positon: fixed;
				    top: 30%;
				    left: 20%;
				    position: fixed;
				}
			.main-bar{
				   width: 80%;
				   margin: 150px 0 0 150%;
				}
        </style>

        <body id=body>

           <header>
        <a href="#" class="logo">Hi.</a>
            <nav>
               <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

			<!-- -------------検索バー---------- -->
			    <form action="ItemSearchResult">
			    	<div class="buscar-caja">
				        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
				        <a class="buscar-btn">
				        	<i class="fas fa-search"></i>
				        </a>
			      </div>
				</form>

<!-- ーーーーーーーーー    上記までは  user html 共有する      ーーーーーーー-->

    <main>
        <div class="left-sidebar">

            <div class="menu">
                <ul>
                    <li class="infomations">
                      <div class="infomation">
                        <img src="img/${user.user_icon}" alt="">
                      </div>
                      <div class="user_name">
                        <p>ようこそ</p>
                      </div>
                      <p class="userName">${user.login_id}　さん</p>
                    </li>
                    <li class="active"><a  href="User?id=${user.id}" class="active">ログイン画面</a></li>
                    <li><a href="UserBuyHistory?id=${user.id}">購入履歴</a></li>
                    <li><a href="UserSellHistory?id=${user.id}">販売履歴</a></li>
                    <li><a href="UserSellChanged?id=${userInfo.id}">作品を販売する</a></li>
                    <li><a href="UserDetail?id=${user.id}">個人情報</a></li>
                    <li><a href="LogOut?id=${user.id}">ログアウト</a></li>
                </ul>
            </div>
        </div>


<!-------user html　では共有する---------->

	<form class="" action="Maneger" method="POST">

	        <div class="main-bar">
	            <table class="table table-striped">
	                <thead>
	                  <tr>
	                    <th scope="col"></th>
	                    <th scope="col">id</th>
	                    <th scope="col">ログインID</th>
	                    <th scope="col">Handle</th>
	                  </tr>
	                </thead>

	                <tbody>
	                <c:forEach var="user" items="${userList}" >
	                  <tr>
	                    <th scope="row"> <div class="infomation">
	                        <img src="img/${user.user_icon}" alt="">
	                      </div></th>
	                    <td>${user.id}</td>
	                    <td>${user.login_id}</td>
	                    <td><a href="User?id=${user.id}" type="button" class="btn  btn-outline-success">詳細</a>

	                     <c:if test="${user.id!=1}">
	                        <a href="UserDelete?id=${user.id}" type="button" class="btn mt-3 btn-outline-danger">削除</a>
	                     </c:if>

	                  </tr>
	                   </c:forEach>
	                </tbody>
	              </table>
	        </div>



        	<div class="buttons-handle">
        		<div><button type="submit" name="confirm_button" class="my-3 mx-2 btn-stitch" value="buier">購入者</button></div>
        		<div><button type="submit" name="confirm_button" class="my-3 mx-2 btn-stitch" value="seller">販売者</button></div>
        		<div><button type="submit" name="confirm_button" class="my-3 mx-2 btn-stitch" value="allUser">全員</button></div>
        	</div>


        </form>

    </main>
    <footer>

      <footer>
        <div class="container footer-row">
            <hr>
            <div class="footer-left-col">
                <div class="footer-links">
                    <div class="link-title">
                        <h4>Hiを知る</h4>
                        <small>Hiについて</small><br>
                        <small>Hiとつながる</small>
                    </div>
                    <div class="link-title">
                        <h4>作品販売について</h4>
                        <small>掲載について</small><br>
                        <small>作品販売の規約</small>
                    </div>
                    <div class="link-title">
                        <h4>ヘルプとガイド</h4>
                        <small>ヘルプ</small><br>
                        <small>ガイド</small>
                    </div>

                </div>
            </div>
            <div class="footer-right-col">
                <div class="footer-info">
                    <div class="copyright-text">
                        <small>suppoe@gmail.com</small><br>
                        <small>copyright 2020 Xypo</small>
                    </div>
                    <div class="footer-logo">
                        <button class="btn-stitch">利用規約</button>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    </footer>
    <script type="text/javascript" src="js/index.js"></script>
    <link rel="stylesheet" href="css/detail.css">
</body>
</html>
