<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>新規登録画面</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/detail.css">
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/new.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">



    </head>
    <body id=body>
        <header>
            <a href="Index" class="logo">Hi.</a>
        </header>

    <main>
        <section class="detail">


            <div class="room-gallery">
                    <div class="col-md-4">
                        <div class="discs">
                            <div class="image">
                                <h2 class="pt-3 user-title">ようこそ</h2>
                                <img src="img/${udb.user_icon}" alt="">
                            </div>
                            <div class="text-new">
                                <h3 class="pt-3 user-title">${udb.login_id} さん</h3>
                                </div>
                        </div>
                    </div>
                </div>

            <div class="room-description">
				<form action="SingUp" method="POST">
                    <h2 class="mt-3 item-title">新規登録画面</h2>
                    <div class="form-row my-4">
                      <div class="form-group col-md-6">
                        <label for="inputlastName">氏名</label>
                        <p>${udb.name}</p>
                       </div>
                      <div class="form-group col-md-6">
                        <label for="inputfirstName">名前</label>
                        <p>${udb.first_name}</p>
                      </div>
                    </div>
                    <div class="form-group my-4 col-md-12">
                        <label for="inputAddress">住所</label>
                        <p>${udb.address1}</p>
                        </div>
                      <div class="form-group my-4 col-md-12">
                        <label for="inputAddress2">住所2</label>
                        <p>${udb.address2}</p>
                      </div>
                      <div class="form-group my-4 col-md-12">
                        <label for="inputPassword">パスワード <i onclick="showpassword()" id="eye" class="fa fa-eye" aria-hidden="true"></i></label>
                        <p>${udb.login_password}</p>
                      </div>                    <div class="form-group my-4 col-md-12">
                      <label for="loginId">ログインID</label>
                      <p>${udb.login_id}</p>
                    </div>
                      <h3 class="my-5">上記の内容で登録してよろしいでしょうか</h3>


							<button class="ml-3 btn-back" type="submit" name="confirm_button" value="cancel">Rewirte</button>
							<input type="hidden" name="hidden1" value="${udb.user_icon}">
							<input type="hidden" name="hidden2" value="${udb.login_id}">
							<input type="hidden" name="hidden3" value="${udb.name}">
							<input type="hidden" name="hidden4" value="${udb.first_name}">
							<input type="hidden" name="hidden5" value="${udb.address1}">
							<input type="hidden" name="hidden6" value="${udb.address2}">
							<input type="hidden" name="hidden7" value="${udb.login_password}">
						</form>

						<form action="SignUpResult" method="POST">
                      	<button class="ml-3 btn-stitch" type="submit" name="confirm_button" value="signup">Sign Up</button>

                      	<input type="hidden" name="hidden1" value="${udb.user_icon}">
						<input type="hidden" name="hidden2" value="${udb.login_id}">
						<input type="hidden" name="hidden3" value="${udb.name}">
						<input type="hidden" name="hidden4" value="${udb.first_name}">
						<input type="hidden" name="hidden5" value="${udb.address1}">
						<input type="hidden" name="hidden6" value="${udb.address2}">
						<input type="hidden" name="hidden7" value="${udb.login_password}">
                  </form>
                </div>
        </section>

    </main>
    <script src="js/index.js"></script>



</body>
</html>
