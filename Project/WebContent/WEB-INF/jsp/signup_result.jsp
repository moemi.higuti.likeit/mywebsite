<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>新規登録完了</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/logout.css">


        <body id=body>

          <header>
              <a href="Index" class="logo">Hi.</a>

          </header>
          <main>
              <section class="logout">
                  <div class="logoutform">
                    <h2>登録ありがとうございました。</h2>
                  </div>

	                  <a class="mt-5 btn-stitch" href="User?id=${user.id}">ログイン画面へ行く</a>

	                  <div class="character-message">
	                      <p id=mesa>ありがとう！ <br>登録してくれて</p>
	                    <img id="img" onmouseover="over()" onmouseleave="leave()" class="character" src="img/piyo.gif" alt="">
	                  </div>
              </section>
          </main>


      <script>
          var mesa =document.getElementById('mesa');
          var img = document.getElementById('img');

          function over(){
            mesa.style.display = "block";
            }
          function leave(){
              mesa.style.display = "none";
          }
      </script>

</body>
</html>
