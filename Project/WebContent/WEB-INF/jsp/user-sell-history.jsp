<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>販売履歴</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/user.css">
        <link rel="stylesheet" href="css/user-his.css">
        <link rel="stylesheet" href="css/button.css">


        <body id=body>

            <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
               <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

			<!-- -------------検索バー---------- -->
			    <form action="ItemSearchResult">
			    	<div class="buscar-caja">
				        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
				        <a class="buscar-btn">
				        	<i class="fas fa-search"></i>
				        </a>
			      </div>
				</form>

<!-- ーーーーーーーーー    上記までは  user html 共有する      ーーーーーーー-->

    <main>
        <div class="left-sidebar">

            <div class="menu">
                <ul>
                    <li class="infomations">
                      <div class="infomation">
                        <img src="img/${user.user_icon}" alt="">
                      </div>
                      <div class="user_name">
                        <p>ようこそ</p>
                      </div>
                      <p>${user.login_id}　さん</p>
                    </li>
                    <li><a  href="User?id=${user.id}" >ログイン画面</a></li>
                    <li><a href="UserBuyHistory?id=${user.id}">購入履歴</a></li>
                    <li  class="active"><a href="UserSellHistory?id=${user.id}" class="active">販売履歴</a></li>
                    <li><a href="UserSellChanged?id=${user.id}">作品を販売する</a></li>
                    <li><a href="UserDetail?id=${user.id}">個人情報</a></li>
                    <li><a href="LogOut?id=${user.id}">ログアウト</a></li>
                </ul>
            </div>
        </div>


<!-------user html　では共有する---------->


        <div class="main-bar">

            <section>
                <h2 class="user-title mb-5">Selling</h2>

                <section>
                  <c:if test="${InfoMessage != null}" >
		           	<h4>${InfoMessage}</h4>
				  </c:if>
				</section>

				<c:if test="${InfoMessage == null}" >
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col"></th>
                        <th scope="col">出品名</th>
                        <th scope="col">出品日時</th>
                        <th scope="col">カテゴリー</th>
                      </tr>
                    </thead>
                    <tbody>

                    <c:forEach var="list" items="${list}" varStatus="status">
                      <tr>
                        <th scope="row"><a href="ItemSellDetailWithComments?item_id=${list.id}"><i class="ml-2 fas fa-angle-down"></i></a></th>
                        <td>${list.item_name}</td>
                        <td>${list.create_date}</td>
                        <!-- ここに商品を　登録した日時を入れたい -->
                        <td>${list.categoryName}</td>
                      </tr>
                      </c:forEach>
                    </tbody>
                  </table>
                  </c:if>
            </section>
        </div>

    </main>
    <footer>

    </footer>
    <script type="text/javascript" src="js/index.js"></script>
    <link rel="stylesheet" href="css/detail.css">
</body>
</html>
