<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>ログイン画面</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/user.css">
        <link rel="stylesheet" href="css/user-his.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/detail.css">

        <body id=body>

          <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

			<!-- -------------検索バー---------- -->
			    <form action="ItemSearchResult">
			    	<div class="buscar-caja">
				        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
				        <a class="buscar-btn">
				        	<i class="fas fa-search"></i>
				        </a>
			      </div>
				</form>

<!-- ーーーーーーーーー    上記までは  user html 共有する      ーーーーーーー-->

    <main>
        <div class="left-sidebar">

            <div class="menu">
                <ul>
                    <li class="infomations">
                      <div class="infomation">
                        <img src="img/${user.user_icon}" alt="">
                      </div>
                      <div class="user_name">
                        <p>ようこそ</p>
                      </div>
                      <p class="userName">${user.login_id}　さん</p>
                    </li>
                    <li class="active"><a  href="User?id=${user.id}" class="active">ログイン画面</a></li>
                    <li><a href="UserBuyHistory?id=${user.id}">購入履歴</a></li>
                    <li><a href="UserSellHistory?id=${user.id}">販売履歴</a></li>
                    <li><a href="UserSellChanged?id=${user.id}">作品を販売する</a></li>
                    <li><a href="UserDetail?id=${user.id}">個人情報</a></li>
                    <li><a href="LogOut?id=${user.id}">ログアウト</a></li>
                </ul>
            </div>
        </div>


<!-------user html　では共有する---------->

        <div class="main-bar">

     <!------おすすめカテゴリー--------------->

		<c:if test="${ManegerMessage != null}" >
			<form   method="post" action="UserDelete">
			 <input type="hidden" name="id" value="${user.id}">
            	<div>
            	<br>
            	<button type="submit" class="mb-2 btn-stitch">このユーザー情報を消す</button>

				</div>
			</form>
		</c:if>

        <section>
            <div class="container">
                <div class="select">
                    <div class="title">
                        <h2 class="item-title mb-5"><small>おすすめカテゴリー</small></h2>

                    </div>
                    <div class="cards">

							<c:forEach var="cate" items="${categoryList}">

	                            <div class="singleBlog">
	                                <img src="img/${cate.category_img}" alt="">
	                                <div class="blogCotent">
	                                    <h3>${cate.category_name}</h3>
	                                    <a href="Category?categoryName=${cate.category_name}" class="btn-stitch">詳細</a>
	                                </div>
	                            </div>
                            </c:forEach>

                    </div>


                </div>
            </div>
        </section>

        <!---------おすすめ紹介---------------->
        <section>
            <div class="container">

                    <div class="title mb-5">
                        <h2 class="item-title">Hi.<small>のおすすめ</small></h2>
                       <p>スタッフが心をつかまれた作品を紹介</p>
                    </div>

                        <div class="rec_cards row">

                            <!------first stard------>
                            <c:forEach var="item" items="${itemList}">
	                            <div class="item-rec-card x-3 col-md-3">
	                               <div class="single-rec-card">
	                                <div class="product-top">
	                                    <img src="img/${item.file_name1}" alt="">
	                                    <div class="overlay">
	                                        <button type="button" class="btn-btn-secondary" onclick="toggle()" title="Add to Favorite">
	                                            <i class="fa fa-heart" id="o" aria-hidden="true"></i>
	                                        </button>
	                                    </div>
	                                </div>
	                                 <a href="ItemDetail?item_id=${item.id}">

	                                        <p class="mt-3">${item.formatPrice} <small>円 </small></p>
	                                        <p>${item.item_name}</p>
	                                        </a>
	                                       <a href="ItemDetail?item_id=${item.id}" class="btn-stitch">詳細</a>

	                           	 </div>
	                            </div>
                            </c:forEach>
              <!----------- done card--------->
                        </div>
                    </div>
                  </div>

    </section>

    </main>

         <input type="hidden" name="id" value="${user.id}">
    <script type="text/javascript" src="js/index.js"></script>

</body>
</html>
