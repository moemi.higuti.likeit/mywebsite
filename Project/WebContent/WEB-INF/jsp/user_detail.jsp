<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>ユーザー個人情報閲覧</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/user.css">
        <link rel="stylesheet" href="css/user-his.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/detail.css">

        <style>
			.icon-preview img{
			      width: 100px;
			      height: 100px;
			      border-radius: 50%;
			      margin: 50px;
			  }
			</style>

	<header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>



    <!-- -------------検索バー---------- -->
    <form action="ItemSearchResult">
    	<div class="buscar-caja">
	        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
	        <a class="buscar-btn">
	        	<i class="fas fa-search"></i>
	        </a>
      </div>
	</form>

<!---------------------------ログイン------------------------>

    <div class="form" id="form">
    <form   action="Login" method="POST">
      <div class="loginform">
        <div class="header">Login Form
            <i id="close" aria-hidden="false" class="far fa-times-circle"></i></div>


            <div class="input-field">
                <input name="loginId" type="text" required>
                <label>login Id</label>
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </div>
            <div class="input-field">
                <input name="password" id="pswrd" type="password" required>
                <label>Password</label>
                <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <div>
                    <button type="submit" class="btn-stitch">Log in</button>
                </div>
            </form>
            <div class="new">
                <a href="SingUp">新規登録</a>
            </div>
        </div>

    </div>
<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

    <main>
        <div class="left-sidebar">

            <div class="menu">
                <ul>
                    <li class="infomations">
                      <div class="infomation">
                        <img src="img/${user.user_icon}" alt="">
                      </div>
                      <div class="user_name">
                        <p>ようこそ</p>
                      </div>
                      <p class="userName">${user.login_id}　さん</p>
                    </li>
                    <li ><a  href="User?id=${user.id}">ログイン画面</a></li>
                    <li ><a href="UserBuyHistory?id=${user.id}">購入履歴</a></li>
                    <li><a href="UserSellHistory?id=${user.id}">販売履歴</a></li>
                    <li><a href="UserSellChanged?id=${user.id}">作品を販売する</a></li>
                    <li class="active"><a href="UserDetail?id=${user.id}"  class="active">個人情報</a></li>
                    <li><a href="LogOut?id=${user.id}">ログアウト</a></li>
                </ul>
            </div>
        </div>


<!-------user html　では共有する---------->

        <div class="main-bar">
            <h2 class="item-title">登録者個人情報 <small>更新日時 ${user.create_date}"</small> </h2>
            <p class="my-3">変更可</p>
                <form  action="UserDetail" method="POST">

                <c:if test="${errMsg != null}" >
				    <div class="alert alert-danger" role="alert">
					  ${errMsg}
					</div>
				</c:if>

                    <div class="form-row my-4">
                      <div class="form-group col-md-6">
                        <label for="inputlastName">氏名</label>
                        <input type="text" value="${user.name}" class="form-control" name="inputlastName">
                      </div>

                      <div class="form-group col-md-6">
                        <label for="inputfirstName">名前</label>
                        <input type="text"  value="${user.first_name}" class="form-control" name="inputfirstName">
					</div>
                    </div>
                    <div class="form-group my-4">
                        <label for="inputAddress">住所</label>
                        <input type="text" value="${user.address1}" class="form-control" name="inputAddress">
                      </div>
                      <div class="form-group my-4">
                        <label for="inputAddress2">住所２</label>
                        <input type="text" value="${user.address2}" class="form-control" name ="inputAddress2">
                      </div>
                      <div class="form-group my-4">
                        <label for="inputPassword">パスワード<i onclick="showpassword()" id="eye" class="ml-2 fa fa-eye" aria-hidden="true"></i></label>
                        <input type="password"  placeholder="${user.login_password}"    class="form-control" name="inputPassword">
                      </div>
                    <div class="form-group my-4">
                      <label for="inputPassword2">パスワード（確認用）</label>
                      <input type="password" placeholder="${user.login_password}" class="form-control" name="inputPassword2">
                    </div>
                    <div class="form-group my-4">
                      <label for="inputLoginId">ログインID</label>
                      <input type="text" value="${user.login_id}" class="form-control" name="inputLoginId">
                    </div>
                    <div class="form-group my-4">
                      <div class="iconpicselect">
                        <label for="pic">アイコン写真</label>

                        <label for="file-ip-1"></label>
                      <input type="file" value="${user.user_icon}" name="icon" class="form-control-file" id="file-ip-1" onchange="showPreview(event);">

                      <div class="icon-preview">
                        <img class="gallery-hightlight"  src="img/${user.user_icon}"  id="file-ip-big-preview">
                      </div>

                      </div>
                      </div>
                    <button type="submit" class="btn-stitch">変更</button>
                    <input type="hidden" name="id" value="${user.id}">
                  </form>

        </div>

    </main>
    <footer>

    </footer>
    <script type="text/javascript" src="js/index.js"></script>

    <script>

      function showpassword(){
                var inputPassword = document.getElementById('inputPassword');
                var eye = document.getElementById('eye');
                if(inputPassword.type === "password") {
                    inputPassword.type = "text";
                    eye.style.color = "#1da1f2";
                }else{
                    inputPassword.type = "password";
                    eye.style.color = "black";
                }
        }

    </script>
</body>
</html>
