<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>登録商品変更</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/detail.css">
        <link rel="stylesheet" href="css/selldetail.css">
        <link rel="stylesheet" href="css/button.css">


      </head>
      <body id=body>

          <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

			<!-- -------------検索バー---------- -->
			    <form action="ItemSearchResult">
			    	<div class="buscar-caja">
				        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
				        <a class="buscar-btn">
				        	<i class="fas fa-search"></i>
				        </a>
			      </div>
				</form>

<!-- ーーーーーーーーー    上記までは  user html 共有する      ーーーーーーー-->

    <main>
        <section class="detail">

        <input type="hidden" name="hidden0" value="${item.creater_id}">
						<input type="hidden" name="hidden4" value="${item.category_id}">

            </div>
            <div class="room-gallery">
                <form class="selldetailform" action="UserSellChanged" method="POST">
                  <h2 class="user-title">商品登録</h2>
                  <div class="form-group col-md-12">
                    <label for="inputAddress">商品名</label>
                   <input name="itemName"value="${item.item_name}" type="text" class="form-control" id="inputAddress" placeholder="商品名">
                  </div>
                  <div class="form-group col-md-12">
                    <label for="exampleFormControlTextarea1">商品詳細</label>
                    <textarea name="detail"value="${item.detail}" class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="inputCity">金額</label>
                      <input name="price" type="text" value="${item.price}" class="form-control" id="inputCity">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputState">カテゴリー</label>
                      <select name="category" id="inputState" class="form-control">
                        <option selected>カテゴリー</option>
                        <option>陶器</option>
                        <option>折り紙</option>
                        <option>手芸</option>
                        <option>手作りマスク</option>
                        <option>アクセサリー</option>
                        <option>その他</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="inputCity">数個</label>
                      <input name="number"  value="${item.number}" type="text" class="form-control" id="inputCity">
                    </div>
                  </div>
                  <div class="input form-group">
                    <div class="title-pics my-2">商品の画像を最大三枚まで入れられます</div>
                    <label for="file-ip-1"></label>
                    <input name="pic1"  value="${item.file_name1}" type="file" class="form-control-file" id="file-ip-1" onchange="showPreview(event);">
                  </div>
                  <div class="input form-group">
                    <label for="file-ip-2"></label>
                    <input name="pic2" value="${item.file_name2}" type="file" class="form-control-file" id="file-ip-2" onchange="showPreview2(event);">
                  </div>
                  <div class="input form-group">
                    <label for="file-ip-3"></label>
                    <input  name="pic3" value="${item.file_name3}" type="file" class="form-control-file" id="file-ip-3" onchange="showPreview3(event);">
                  </div>
                  <button type="submit" class="mt-3 mr-2 btn-stitch">確認</button>
                  <input type="hidden" name="id" value="${user.id}">
                </form>
                <a href="User" class="btn-back">戻る</a>
          </div>
          <div class="room-description">
            <img class="gallery-hightlight" id="file-ip-big-preview" src="img/piyo.gif" title="画像を入れてください">
            <div class="room-preview">
                <img src="img/${item.file_name1}" id="file-ip-1-preview" class="room-active" alt="" />
                <img src="${item.file_name2}" id="file-ip-2-preview" alt="" />
                <img src="${item.file_name3}" id="file-ip-3-preview" alt="" />
            </div>
        </div>
        </section>

    </main>
    <footer>



    </footer>
    <script type="text/javascript" src="js/index.js"></script>

    <script>

    </script>


</body>
</html>
