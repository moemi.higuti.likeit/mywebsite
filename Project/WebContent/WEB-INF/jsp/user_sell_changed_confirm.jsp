<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>商品登録確認</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/detail.css">
        <link rel="stylesheet" href="css/itemdetail.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">


</head>
<body id=body>

    <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
               <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

    <div class="buscar-caja">
        <input type="text" name="" class="buscar-txt" placeholder="検索">
        <a class="buscar-btn">
        <i class="fas fa-search"></i>
        </a>
      </div>


<!---------------------------ログイン------------------------>

    <div class="form" id="form">
      <div class="loginform">
        <div class="header">Login Form
            <i id="close" aria-hidden="false" class="far fa-times-circle"></i></div>

        <form action="">
            <div class="input-field">
                <input type="text" required>
                <label>login Id</label>
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </div>
            <div class="input-field">
                <input id="pswrd" type="password" required>
                <label>Password</label>
                <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <div>
                    <button type="submit" class="btn-stitch">Log in</button>
                </div>
            </form>
            <div class="new">
                <a href="#">新規登録</a>
            </div>
        </div>
    </div>
    <!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

    <main>
        <section class="detail">
            <div class="room-description">
                <div class="item-title my-5">
                    <h2 class=" ">${item.item_name} さん</h2>
                </div>
                <div class="items-detail">
                    <p>${user.login_id}</p>
                    <table class="table">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">カテゴリー</th>
                            <th scope="col">金額</th>
                            <th scope="col">残りの数</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row"></th>
                            <td>${itemCate}</td>
                            <td>${item.price}　円</td>
                            <td>${item.number}　個</td>
                          </tr>

                        </tbody>
                      </table>
                        <p class="item-detail">${item.detail}</p>
                </div>
            </div>
            <div class="room-gallery">
                <img class="gallery-hightlight" src="img/${item.file_name1}" alt="room1" />
                <div class="room-preview">
                    <img src="img/${item.file_name1}" class="room-active" alt="" />
                    <img src="img/${item.file_name2}"  alt="" />
                    <img src="img/${item.file_name3}"  alt="" />
                </div>
            </div>
        </section>

        <section>
            <div class="confirm">
                <h3>本当に登録してよろしいでしょうか？</h3>
                <form action="ItemInsertResult" method="POST">
                      	 <button type="submit" class="mt-3 btn-stitch">登録</button>

						<input type="hidden" name="hidden0" value="${item.creater_id}">
                      	<input type="hidden" name="hidden1" value="${item.item_name}">
						<input type="hidden" name="hidden2" value="${item.detail}">
						<input type="hidden" name="hidden3" value="${item.price}">
						<input type="hidden" name="hidden4" value="${item.category_id}">
						<input type="hidden" name="hidden5" value="${item.file_name1}">
						<input type="hidden" name="hidden6" value="${item.file_name2}">
						<input type="hidden" name="hidden7" value="${item.file_name3}">
						<input type="hidden" name="hidden8" value="${item.number}">
						 <input type="hidden" name="id" value="${user.id}">
                  </form>


                  <form action="UserSellChangedCon" method="POST">
                      	<button type="submit" class="btn-back">戻る</button>

                      	<input type="hidden" name="hidden0" value="${item.creater_id}">
                      	<input type="hidden" name="hidden1" value="${item.item_name}">
						<input type="hidden" name="hidden2" value="${item.detail}">
						<input type="hidden" name="hidden3" value="${item.price}">
						<input type="hidden" name="hidden4" value="${item.category_id}">
						<input type="hidden" name="hidden5" value="${item.file_name1}">
						<input type="hidden" name="hidden6" value="${item.file_name2}">
						<input type="hidden" name="hidden7" value="${item.file_name3}">
						<input type="hidden" name="hidden8" value="${item.number}">
						 <input type="hidden" name="id" value="${user.id}">
						 <input type="hidden" name="cate" value="${itemCate}">

                  </form>
            </div>

        </section>


    </main>

    <script type="text/javascript" src="js/index.js"></script>

</body>
</html>