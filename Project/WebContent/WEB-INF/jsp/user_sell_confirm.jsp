<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>商品登録確認</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/detail.css">
        <link rel="stylesheet" href="css/itemdetail.css">
        <link rel="stylesheet" href="css/button.css">
        <link rel="stylesheet" href="css/footer.css">


</head>
<body id=body>

    <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
                <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>

                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>

    <div class="buscar-caja">
        <input type="text" name="" class="buscar-txt" placeholder="検索">
        <a class="buscar-btn">
        <i class="fas fa-search"></i>
        </a>
      </div>



    </div>
    <!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

    <main>
        <section class="detail">
            <div class="room-description">
                <div class="item-title my-5">
                    <h2 class=" ">商品タイトル</h2>
                </div>
                <div class="items-detail">
                    <p>すごい人の作品　作品者　の名前が入るよ</p>
                    <table class="table">
                        <thead class="thead-dark">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">カテゴリー</th>
                            <th scope="col">金額</th>
                            <th scope="col">残りの数</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row"></th>
                            <td>アクセサリー</td>
                            <td>100000000000　円</td>
                            <td>1　個</td>
                          </tr>

                        </tbody>
                      </table>
                        <p class="item-detail">商品詳細が入る本革レザーの子ども用のソファになります。
                        ※こちらの商品はソファとクッションのみのお値段です。
                        大人のソファと同じ作りで、職人が1つ1つ丁寧に制作しておりますので、長く愛用して頂けます。
                        可愛らしいサイズのソファですが、インテリアとしてもおしゃれに使って</p>
                </div>
            </div>
            <div class="room-gallery">
                <img class="gallery-hightlight" src="img/flower.jpg" alt="room1" />
                <div class="room-preview">
                    <img src="img/flower.jpg" class="room-active" alt="" />
                    <img src="img/805909_s.jpg"  alt="" />
                    <img src="img/eggs.jpg"  alt="" />
                </div>
            </div>
        </section>

        <section>
            <div class="confirm">
                <h3>本当に登録してよろしいでしょうか？</h3>
                <button type="submit" class="mt-3 btn-stitch">登録</button>
                <button type="submit" class="btn-back">戻る</button>
            </div>

        </section>


    </main>

    <script type="text/javascript" src="js/index.js"></script>

</body>
</html>
