<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <title>作品詳細</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/slick.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="css/slick-theme.css" media="screen" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/slick.min.js"></script>
        <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/detail.css">
        <link rel="stylesheet" href="css/itemdetail.css">
        <link rel="stylesheet" href="css/button.css">

		<style>
		.items-detail{
			width:80%;
		}
		.buttonback{
			margin:150px;
		}
		.item-detail {
		    margin-bottom: 50px;
		    }
		</style>

    </head>
    <body id=body>

       <header>
        <a href="Index" class="logo">Hi.</a>
            <nav>
               <ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>
                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
            </nav>
            <div class="menu-toggle"><i class="fas fa-bars"></i></div>
    </header>



    <!-- -------------検索バー---------- -->
    <form action="ItemSearchResult">
    	<div class="buscar-caja">
	        <input type="text" name="search_word" class="buscar-txt" placeholder="検索">
	        <a class="buscar-btn">
	        	<i class="fas fa-search"></i>
	        </a>
      </div>
	</form>

<!---------------------------ログイン------------------------>

    <div class="form" id="form">
    <form   action="Login" method="POST">
      <div class="loginform">
        <div class="header">Login Form
            <i id="close" aria-hidden="false" class="far fa-times-circle"></i></div>


            <div class="input-field">
                <input name="loginId" type="text" required>
                <label>login Id</label>
                <i class="fa fa-user-circle" aria-hidden="true"></i>
            </div>
            <div class="input-field">
                <input name="password" id="pswrd" type="password" required>
                <label>Password</label>
                <i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
            </div>
            <div>
                    <button type="submit" class="btn-stitch">Log in</button>
                </div>
            </form>
            <div class="new">
                <a href="SingUp">新規登録</a>
            </div>
        </div>

    </div>
<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

    <main>
        <section class="detail">
            <div class="room-description">
                <section class="detail">
            <div class="room-description">
                   <form class="">
                    <div class="center">
                        <div class="stars">
                          <input type="radio" id="five" name="rate" value="5">
                          <label for="five"></label>
                          <input type="radio" id="four" name="rate" value="4">
                          <label for="four"></label>
                          <input type="radio" id="three" name="rate" value="3">
                          <label for="three"></label>
                          <input type="radio" id="two" name="rate" value="2">
                          <label for="two"></label>
                          <input type="radio" id="one" name="rate" value="1">
                          <label for="one"></label>
                          <span class="result"></span>
                        </div>
                    </div>
                          <div class="form-group">
                            <label class="postFromBuier" for="exampleFormControlTextarea1">ご購入された感想を入力してください</label>

                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                          </div>
                        <div class="form-group">
                          <div class="col-sm-10">
                            <button type="submit" class="post btn-stitch">送信</button>
                          </div>
                        </div>
                      </form>
              </div>
            <div class="room-gallery">
                <img class="gallery-hightlight" src="img/${item.file_name1}" alt="main_pcture" />
                <div class="room-preview">
                    <img src="img/${item.file_name1}"  class="room-active" alt="" />
                    <img src="img/${item.file_name2}"   alt="" />
                    <img src="img/${item.file_name3}"   alt="" />
                </div>
            </div>
        </section>

    </main>

    <script type="text/javascript" src="js/index.js"></script>

</body>
</html>
