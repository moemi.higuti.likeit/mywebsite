<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<title>お買い求め確認</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/slick.css"
	media="screen" />
<link rel="stylesheet" type="text/css" href="css/slick-theme.css"
	media="screen" />
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="js/slick.min.js"></script>
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/user_buy_confirm.css">
<link rel="stylesheet" href="css/button.css">
<link rel="stylesheet" href="css/footer.css">


</head>
<body id=body>

	<header>
		<a href="Index" class="logo">Hi.</a>
		<nav>
			<ul>
                    <li  class="submenu"><a href="#">カテゴリー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="Category?categoryName=陶器">陶器</a></li>
                          <li><a href="Category?categoryName=折り紙">折り紙</a></li>
                          <li><a href="Category?categoryName=手芸">手芸</a></li>
                          <li><a href="Category?categoryName=手作りマスク">手作りマスク</a></li>
                          <li><a href="Category?categoryName=アクセサリー">アクセサリー</a></li>
                          <li><a href="Category?categoryName=その他">その他</a></li>
                        </ul>
                    </li>
                    <li  class="submenu"><a href="#">★レビュー<i class="ml-2 fas fa-angle-down"></i></a>
                        <ul>
                       	  <li><a href="ItemSearchByrating?score=1">★１つ</a></li>
                          <li><a href="ItemSearchByrating?score=2">★２つ</a></li>
                          <li><a href="ItemSearchByrating?score=3">★３つ</a></li>
                          <li><a href="ItemSearchByrating?score=4">★４つ</a></li>
                          <li><a href="ItemSearchByrating?score=5">★５つ</a></li>
                        </ul>
                    </li>
                    <c:if test="${userInfo.id==1 && user.id!=NULL}">
		                <li id="login"><a class="login" href="Maneger?userInfo.id=${userInfo.id}" >管理者画面</a></li>
					</c:if>
					<c:if test="${user.id==NULL}">
		                <li id="login"><a class="login" href="#" >ログイン</a></li>
                    <li> <a href="SingUp">新規登録</a></li>
					</c:if>
					<c:if test="${user.id!=NULL}">
		               <li id="login"><a class="login" href="User?id=${user.id}" >ログイン画面へ</a></li>
		                <li><a href="Cart">カート</a></li>
					</c:if>

                </ul>
		</nav>
		<div class="menu-toggle">
			<i class="fas fa-bars"></i>
		</div>
	</header>

	<div class="buscar-caja">
		<input type="text" name="" class="buscar-txt" placeholder="検索">
		<a class="buscar-btn"> <i class="fas fa-search"></i>
		</a>
	</div>


	<!---------------------------ログイン------------------------>

	<div class="form" id="form">
		<div class="loginform">
			<div class="header">
				Login Form <i id="close" aria-hidden="false"
					class="far fa-times-circle"></i>
			</div>

			<form action="">
				<div class="input-field">
					<input type="text" required> <label>login Id</label> <i
						class="fa fa-user-circle" aria-hidden="true"></i>
				</div>
				<div class="input-field">
					<input id="pswrd" type="password" required> <label>Password</label>
					<i onclick="show()" class="fa fa-eye" aria-hidden="true"></i>
				</div>
				<div>
					<button type="submit" class="btn-stitch">Login</button>
				</div>
			</form>
			<div class="new">
				<a href="#">新規登録</a>
			</div>
		</div>
	</div>
	<!-- ーーーーーーーーー    上記までは共有する      ーーーーーーー         -->

	<main>
	<section>
		<h2 class="my-5 buy_confirm_title">${user.login_id}さんカートの中身</h2>
		<div>${cartActionMessage}</div>

		<input type="hidden" name="id" value="${user.id}">
		<table class="table_buy">
			<thead class="thead-light">
				<tr>
					<th scope="col">作品名</th>
					<th scope="col">購入数</th>
					<th scope="col">販売価格 <small>税込み</small></th>
				</tr>
			</thead>

			<!-- ここから挿入していくよ -->
			<c:forEach var="item" items="${cart}" varStatus="status">


				<tbody>
					<tr>
						<th scope="row">
							<div class="item_title">
								<img class="confirm_img" src="img/${item.file_name1}" alt="">
								<p>${item.item_name}</p>
							</div>
						</th>
						<td>${item.buyNumber} 　 点</td>

						<td>${item.formatTotalPrice} <small>円</small> </td>

					</tr>
			</c:forEach>

			</tbody>

			<tbody>
				<tr>
					<th scope="row"></th>
					<!-- 配送方法の情報の取得と入力 -->

					<form action="UserBuyFinallyConfirm" method="post">
					<input type="hidden" name="buyNumber" value="${item.buyNumber}">

						<td class="delivary_title">配送方法</td>
						<td>
							<select name="delivery_method_id">
								<c:forEach var="dmdb" items="${dmdbList}">
									<option value="${dmdb.id}">${dmdb.name}</option>
								</c:forEach>
							</select>
						</td>
				</tr>
			</tbody>
		</table>
	</section>

	<section>
				<button type="submit" name="action" class="btn-stitch">確認</button>
				</form>
		<a href="Cart" class="btn-back">戻る
	</section>
	</main>

	<script type="text/javascript" src="js/index.js"></script>



</body>
</html>
