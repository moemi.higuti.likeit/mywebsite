package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyBeans implements Serializable{

	private int id;
	private int user_id;
	private int total_price;
	private int delivery_method_id;
	private Date create_date;

	private String deliveryMethodName;
	private int deliveryMethodPrice;






	public String getDeliveryMethodName() {
		return deliveryMethodName;
	}


	public void setDeliveryMethodName(String deliveryMethodName) {
		this.deliveryMethodName = deliveryMethodName;
	}


	public int getDeliveryMethodPrice() {
		return deliveryMethodPrice;
	}


	public void setDeliveryMethodPrice(int deliveryMethodPrice) {
		this.deliveryMethodPrice = deliveryMethodPrice;
	}


	public BuyBeans() {

	}


	public BuyBeans(int id, int user_id, int total_price, int delivery_method_id, Date create_date) {

		this.id = id;
		this.user_id = user_id;
		this.total_price = total_price;
		this.delivery_method_id = delivery_method_id;
		this.create_date = create_date;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getTotal_price() {
		return total_price;
	}
	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
	public int getDelivery_method_id() {
		return delivery_method_id;
	}
	public void setDelivery_method_id(int delivery_method_id) {
		this.delivery_method_id = delivery_method_id;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getFormatTotalPrice() {
		return String.format("%,d", this.total_price);
	}
	public String getFormatPrice() {
		return String.format("%,d", this.total_price);
	}
	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(create_date);
	}

}
