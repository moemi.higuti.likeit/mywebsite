package beans;

import java.io.Serializable;

public class Buy_detailBeans implements Serializable {

	private int id;
	private int buy_id;
	private int item_id;
	private int category_id;
	private int number;



	public Buy_detailBeans() {

	}


	public Buy_detailBeans(int id, int buy_id, int item_id, int category_id, int number) {

		this.id = id;
		this.buy_id = buy_id;
		this.item_id = item_id;
		this.category_id = category_id;
		this.number = number;
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBuy_id() {
		return buy_id;
	}
	public void setBuy_id(int buy_id) {
		this.buy_id = buy_id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}





}
