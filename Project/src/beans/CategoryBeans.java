package beans;

import java.io.Serializable;

public class CategoryBeans implements Serializable {

	private int id;
	private String category_name;
	private String category_img;



public CategoryBeans(int id, String category_name, String category_img) {

		this.id = id;
		this.category_name = category_name;
		this.category_img = category_img;
	}


//コンストラクタ



	public CategoryBeans() {

	}




	public CategoryBeans(int id) {
		this.id = id;
	}


	//アクセサ
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}


	public String getCategory_img() {
		return category_img;
	}


	public void setCategory_img(String category_img) {
		this.category_img = category_img;
	}



}
