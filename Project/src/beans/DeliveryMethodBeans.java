package beans;

import java.io.Serializable;

public class DeliveryMethodBeans implements Serializable  {

	private int id;
	private String name;
	private int price;


	//空引数
	public DeliveryMethodBeans() {

		}

	//全要素
	public DeliveryMethodBeans(int id, String name, int price) {

		this.id = id;
		this.name = name;
		this.price = price;
	}
//アクセサ

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}


}
