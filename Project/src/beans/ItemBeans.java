package beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ItemBeans implements Serializable {
	private int id;
	private int creater_id;
	private String item_name;
	private String detail;
	private int price;
	private int category_id;
	private String file_name1;
	private String file_name2;
	private String file_name3;
	private int number;
	private String categoryName;
	private String createrName;
	private Date create_date;
	private int buyNumber;
	private int totalPrice;




//アクセサ

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCreater_id() {
		return creater_id;
	}
	public void setCreater_id(int creater_id) {
		this.creater_id = creater_id;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getFile_name1() {
		return file_name1;
	}
	public void setFile_name1(String file_name1) {
		this.file_name1 = file_name1;
	}
	public String getFile_name2() {
		return file_name2;
	}
	public void setFile_name2(String file_name2) {
		this.file_name2 = file_name2;
	}
	public String getFile_name3() {
		return file_name3;
	}
	public void setFile_name3(String file_name3) {
		this.file_name3 = file_name3;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}

	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}

	public int getBuyNumber() {
		return buyNumber;
	}

	public void setBuyNumber(int buyNumber) {
		this.buyNumber = buyNumber;
	}

	public Date getCreatedate() {
		return create_date;
	}

	public void setCreatedate(Date createdate) {
		this.create_date = createdate;
	}
	public String getCreaterName() {
		return createrName;
	}

	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}

	public Date getCreate_date() {
		return create_date;
	}

	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}


	public String getFormatDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		return sdf.format(create_date);
	}

	public String getCategoryName() {
		return categoryName;
	}

	public List<ItemBeans> setCategoryName(String categoryName) {
		this.categoryName = categoryName;
		return null;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int i) {
		this.totalPrice = i;
	}

	public String getFormatTotalPrice() {
		return String.format("%,d", this.totalPrice);
	}



}
