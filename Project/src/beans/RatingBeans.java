package beans;

import java.sql.Date;

import javafx.scene.chart.PieChart.Data;

public class RatingBeans {

	public int id;
	public int item_id;
	public int score;
	public String coment;
	public Data create_date;
	public int userid;
	public String userIcon;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getItem_id() {
		return item_id;
	}
	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getComent() {
		return coment;
	}
	public void setComent(String coment) {
		this.coment = coment;
	}
	public Data getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Data create_date) {
		this.create_date = create_date;
	}
	public void setCreate_date(Date date) {


	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUserIcon() {
		return userIcon;
	}
	public void setUserIcon(String userIcon) {
		this.userIcon = userIcon;
	}

}
