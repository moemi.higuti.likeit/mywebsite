package beans;

import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable {

	private int id;
	private String name;

	private String address1;
	private String address2;
	private String login_id;
	private String login_password;
	private String user_icon;
	private Date create_date;
	private String first_name;


	public UserBeans() {

	}



	public UserBeans(int id, String name, String address1, String address2, String login_id, String login_password,
			String user_icon, Date create_date, String first_name) {

		this.id = id;
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.login_id = login_id;
		this.login_password = login_password;
		this.user_icon = user_icon;
		this.create_date = create_date;
		this.first_name = first_name;
	}






	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getLogin_id() {
		return login_id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public String getLogin_password() {
		return login_password;
	}
	public void setLogin_password(String login_password) {
		this.login_password = login_password;
	}
	public String getUser_icon() {
		return user_icon;
	}
	public void setUser_icon(String user_icon) {
		this.user_icon = user_icon;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}




}
