package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyBeans;

public class BuyDao {

	/**
	 * 購入情報登録処理
	 */
	public static int insertBuy(BuyBeans bdb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy(user_id,total_price,delivery_method_id,create_date) VALUES (?,?,?,now())",
					Statement.RETURN_GENERATED_KEYS);

			st.setInt(1, bdb.getUser_id());
			st.setInt(2, bdb.getTotal_price());
			st.setInt(3, bdb.getDelivery_method_id());

			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * ユーザーの購入商品の大まかな情報を入手
	 *
	 * */
	public static List<BuyBeans> getBuyDataBeansByBuyIdList(String user_id) {
		Connection con = null;
		PreparedStatement st = null;

		ArrayList<BuyBeans> list = new ArrayList<BuyBeans>();

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM t_buy JOIN `m_delivery_method` ON t_buy.delivery_method_id = m_delivery_method.id WHERE t_buy.user_id = ?;");

			st.setString(1, user_id);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				BuyBeans bdb = new BuyBeans();

				bdb.setId(rs.getInt("id"));
				bdb.setUser_id(rs.getInt("user_id"));
				bdb.setDelivery_method_id(rs.getInt("delivery_method_id"));
				bdb.setTotal_price(rs.getInt("total_price"));
				bdb.setCreate_date(rs.getTimestamp("create_date"));

				list.add(bdb);
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				throw new SQLException(e);
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
			}
		}
		return list;
	}


	//idで個人の購入を取得する

	public BuyBeans getBuyDataBeansByBuyIdNId(String Id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM `t_buy` JOIN `m_delivery_method`  ON t_buy.delivery_method_id = m_delivery_method.id WHERE t_buy.id = ?");

			st.setString(1, Id);

			ResultSet rs = st.executeQuery();

			BuyBeans bdb = new BuyBeans();
			if (rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setUser_id(rs.getInt("user_id"));
				bdb.setDelivery_method_id(rs.getInt("delivery_method_id"));
				bdb.setTotal_price(rs.getInt("total_price"));
				bdb.setCreate_date(rs.getTimestamp("create_date"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				bdb.setDeliveryMethodName(rs.getString("name"));
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

			return bdb;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
