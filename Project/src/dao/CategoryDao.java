package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.CategoryBeans;

public class CategoryDao {

	private static final CategoryBeans cate = null;

	public CategoryDao(int id) {
		// TODO 自動生成されたコンストラクター・スタブ
	}

	/**
	 * ランダムで引数指定分のおすすめカテゴリーを四つを入手
	 * @param limit 取得したいかず
	 */

	public static ArrayList<CategoryBeans> getCategory() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		try {
			//DBとの接続
			con = DBManager.getConnection();

			//実行したいSQLを入れる　要確認
			st = con.prepareStatement("SELECT * FROM `t_category` ORDER BY RAND() LIMIT 4");

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<CategoryBeans> categoryList = new ArrayList<CategoryBeans>();

			while (rs.next()) {

				CategoryBeans cate = new CategoryBeans();

				cate.setId(rs.getInt("id"));
				cate.setCategory_name(rs.getString("category_name"));
				cate.setCategory_img(rs.getString("category_img"));

				categoryList.add(cate);
			}

			System.out.println("getAllItem completed");
			return categoryList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);

		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * idでカテゴリー名を取得
	 * RAND()　ランダム関数
	 */

	public static CategoryBeans getCategoryByID(Object CategoryId) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			//DBとの接続
			con = DBManager.getConnection();

			//実行したいSQLを入れる　要確認
			st = con.prepareStatement("SELECT * FROM `t_category` where id =?");
			st.setObject(1, CategoryId);

			ResultSet rs = st.executeQuery();

			CategoryBeans cate = new CategoryBeans();

			if (rs.next()) {

				cate.setId(rs.getInt("id"));
				cate.setCategory_name(rs.getString("category_name"));
				cate.setCategory_img(rs.getString("category_img"));

			}

			System.out.println("getAllItem completed");
			return cate;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				throw new SQLException(e);
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}

		} finally {
			if (con != null) {

			}
		}
		return cate;

	}

	/**
	 * カテゴリー名でかあてごりーIDを取得
	 * 商品入力で使う
	 */

	public static CategoryBeans getCategoryByName(String itemCate) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			//DBとの接続
			con = DBManager.getConnection();

			//実行したいSQLを入れる　要確認
			st = con.prepareStatement("select * from `t_category` where `category_name` = ?");
			st.setObject(1, itemCate);

			ResultSet rs = st.executeQuery();

			CategoryBeans cate = new CategoryBeans();

			if (rs.next()) {

				cate.setId(rs.getInt("id"));
				cate.setCategory_name(rs.getString("category_name"));
				cate.setCategory_img(rs.getString("category_img"));

			}

			System.out.println("getAllItem completed");
			return cate;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				throw new SQLException(e);
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}

		} finally {
			if (con != null) {

			}
		}
		return cate;

	}

	/**
	 * カテゴリー名でかあてごりーIDを取得
	 * 商品入力で使う
	 */

	public static CategoryBeans getCategoryById(int id) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			//DBとの接続
			con = DBManager.getConnection();

			//実行したいSQLを入れる　要確認
			st = con.prepareStatement("select * from `t_category` where id = ?");
			st.setInt(1, id);

			ResultSet rs = st.executeQuery();

			CategoryBeans cate = new CategoryBeans();

			while (rs.next()) {
				cate.setId(rs.getInt("id"));
				cate.setCategory_name(rs.getString("category_name"));
				cate.setCategory_img(rs.getString("category_img"));
			}

			System.out.println("getAllItem completed");
			return cate;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				throw new SQLException(e);
			} catch (SQLException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}

		} finally {
			if (con != null) {

			}
		}
		return cate;

	}

}
