package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ItemBeans;

public class ItemDao {

	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @throws SQLException
	 * RAND()　ランダム関数
	 */

	public static ArrayList<ItemBeans> getRandItemShow(int limit) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			//DBとの接続
			con = DBManager.getConnection();

			//実行したいSQLを入れる　要確認
			st = con.prepareStatement("SELECT * FROM `t-item` where `number` not in (0) ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCreater_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				itemList.add(item);
			}

			System.out.println("getAllItem completed");

			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品検索
	 * @param searchWord
	 * @param pageNum
	 * @param pageMaxItemCount
	 * @return
	 * @throws SQLException
	 */

	public static ArrayList<ItemBeans> getItemsByItemName(String searchWord, int pageNum, int pageMaxItemCount)
			throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			if (searchWord.length() == 0) {
				// 全検索
				st = con.prepareStatement(
						"SELECT * FROM `t-item` where `number` not in (0) ORDER BY id ASC LIMIT ?,? ");
				st.setInt(1, startiItemNum);
				st.setInt(2, pageMaxItemCount);
			} else {
				// 商品名検索 and name like  '%" + name + "%'";
				st = con.prepareStatement(
						"SELECT * FROM `t-item` WHERE item_name like ? and `number` not in (0) ORDER BY id ASC LIMIT ?,?");

				st.setString(1, "%" + searchWord + "%");
				st.setInt(2, startiItemNum);
				st.setInt(3, pageMaxItemCount);
			}

			ResultSet rs = st.executeQuery();
			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCreater_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品総数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getItemCount(String searchWord) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			//あいまい検索語の数を数える
			st = con.prepareStatement(
					"select count(*) as cnt from `t-item` where item_name like ? and `number` not in (0)");

			st.setString(1, "%" + searchWord + "%");

			ResultSet rs = st.executeQuery();

			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * カテゴリーネームで
	 * 商品の総取得数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getItemCountByCategoryName(String categoryName) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			//カテゴリー検索語の数を数える
			st = con.prepareStatement(
					"select count(*) as cnt from `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id  where `t_category`.category_name =? and `number` not in (0)");

			st.setString(1, categoryName);

			ResultSet rs = st.executeQuery();

			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索
	 *　情報詳細に使う
	 * @param itemId
	 * @return ItemBeans
	 * @throws SQLException
	 */

	public static ItemBeans getItemByItemID(int itemId) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id where `t-item`.id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemBeans item = new ItemBeans();

			if (rs.next()) {

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				System.out.println("searching item by itemID has been completed");
			}

			return item;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 商品IDによる商品検索
	 *　情報詳細に使う
	 * @param itemId
	 * @return ItemBeans
	 * @throws SQLException
	 */

	public static ArrayList<ItemBeans> getItemsByStringCategoryName(String categoryName, int pageNum,
			int pageMaxItemCount) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id where `t_category`.category_name = ? and `number` not in (0) ORDER BY `t-item`.id ASC LIMIT ?,?");

			st.setString(1, categoryName);
			st.setInt(2, startiItemNum);
			st.setInt(3, pageMaxItemCount);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCreater_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");

			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * 商品入力
	 *
	 *
	 * */

	public static void InsertItem(ItemBeans itemInsert) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"INSERT INTO `t-item` ( `creater_id`, `item_name`, `detail`,`price`, `category_id`, `file_name1`, `file_name2`, `file_name3`, `number`,`create_date`) VALUES (?,?,?,?,?,?,?,?,?,now())");

			st.setInt(1, itemInsert.getCreater_id());
			st.setString(2, itemInsert.getItem_name());
			st.setString(3, itemInsert.getDetail());
			st.setInt(4, itemInsert.getPrice());
			st.setInt(5, itemInsert.getCategory_id());
			st.setString(6, itemInsert.getFile_name1());
			st.setString(7, itemInsert.getFile_name2());
			st.setString(8, itemInsert.getFile_name3());
			st.setInt(9, itemInsert.getNumber());

			st.executeUpdate();

			System.out.println("Iteminserting has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * クリエーターIDによる
	 * 販売商品の情報の取得
	 *
	 */

	public static List<ItemBeans> getSellingItemByCreaterID(String CreaterID) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		ArrayList<ItemBeans> list = new ArrayList<ItemBeans>();

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM `t-item`  WHERE creater_id = ?");
			st.setString(1, CreaterID);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCategory_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				list.add(item);
			}

			System.out.println("searching item by itemID has been completed");

			return list;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * 購入した商品を
	 * 元の数から減らしていく
	 * */

	public static void getItemRestUpdate(ItemBeans cart) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("UPDATE `t-item` SET `number` = ? - ? WHERE id = ?");

			st.setInt(1, cart.getNumber());
			st.setInt(2, cart.getBuyNumber());
			st.setInt(3, cart.getId());

			st.executeUpdate();

			System.out.println(cart.getNumber() + "-------" + cart.getBuyNumber());

			System.out.println("Item rest has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * 売り切れ商品の
	 * 情報を入手
	 *
	 *
	 * */

	public static ArrayList<ItemBeans> getSoldOutItems(int pageNum, int pageMaxItemCount) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM `t-item`  where `number` = 0 ORDER BY `t-item`.id ASC LIMIT ?,?");

			st.setInt(1, startiItemNum);
			st.setInt(2, pageMaxItemCount);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCreater_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				itemList.add(item);
			}
			System.out.println("get Items by itemName has been completed");

			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * soldoutの商品の数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getSoldOutItemCount() throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			//あいまい検索語の数を数える
			st = con.prepareStatement("select count(*) as cnt from `t-item` where `number` = 0");

			ResultSet rs = st.executeQuery();

			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	public static List<ItemBeans> getCosNItem(String Id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;

		ArrayList<ItemBeans> list = new ArrayList<ItemBeans>();

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM `t_buy_detail` JOIN `t-item` ON `t_buy_detail`.item_id = `t-item`.id JOIN t_buy ON t_buy_detail.buy_id = t_buy.id  where buy_id = ? ORDER BY price ASC");

			st.setString(1, Id);

			ResultSet rs = st.executeQuery();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("item_id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCreater_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));
				item.setBuyNumber(rs.getInt("number"));
				item.setTotalPrice(rs.getInt("number") * rs.getInt("price"));

				list.add(item);
			}

			System.out.println("searching BuyDataBeans by buyID has been completed");

			return list;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}



	/**
	 * レビューの星の数で
	 * 商品の総取得数を取得
	 *
	 * @param searchWord
	 * @return
	 * @throws SQLException
	 */
	public static double getItemCountByRating(String score) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			//カテゴリー検索語の数を数える
			st = con.prepareStatement(
					"select count(*) as cnt from `t-item` JOIN rating ON `t-item`.id = `rating`.item_id  where `rating`.score =? and `number` not in (0)");

			st.setString(1, score);

			ResultSet rs = st.executeQuery();

			double coung = 0.0;
			while (rs.next()) {
				coung = Double.parseDouble(rs.getString("cnt"));
			}
			return coung;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}








	/**
	 * 商品IDによる商品検索
	 *　情報詳細に使う
	 * @param itemId
	 * @return ItemBeans
	 * @throws SQLException
	 */

	public static ArrayList<ItemBeans> getItemsByRating(String score, int pageNum,
			int pageMaxItemCount) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			int startiItemNum = (pageNum - 1) * pageMaxItemCount;
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"select  DISTINCT  `t-item`.id,`t-item`.creater_id,`t-item`.item_name,`t-item`.detail,"
					+ "`t-item`.price,`t-item`.category_id,`t-item`.file_name1,`t-item`.file_name2,`t-item`.file_name3,"
					+ "`t-item`.number,`t-item`.create_date from `t-item` "
					+ "JOIN rating  ON `t-item`.id = `rating`.item_id where score =?"
					+ "and `number` not in (0) ORDER BY `t-item`.id ASC LIMIT ?,?");

			st.setString(1, score);
			st.setInt(2, startiItemNum);
			st.setInt(3, pageMaxItemCount);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemBeans> itemList = new ArrayList<ItemBeans>();

			while (rs.next()) {

				ItemBeans item = new ItemBeans();

				item.setId(rs.getInt("id"));
				item.setCreater_id(rs.getInt("creater_id"));
				item.setItem_name(rs.getString("item_name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setCreater_id(rs.getInt("category_id"));
				item.setFile_name1(rs.getString("file_name1"));
				item.setFile_name2(rs.getString("file_name2"));
				item.setFile_name3(rs.getString("file_name3"));
				item.setNumber(rs.getInt("number"));
				item.setCreatedate(rs.getDate("create_date"));

				itemList.add(item);

			}


			System.out.println("get Items by itemName has been completed");

			return itemList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
























}
//
//
//
//
//
//
//while (rs.next()) {
//
//	ItemBeans item = new ItemBeans();
//
//	item.setId(rs.getInt("item_id"));
//	item.setCreater_id(rs.getInt("creater_id"));
//	item.setItem_name(rs.getString("item_name"));
//	item.setDetail(rs.getString("detail"));
//	item.setPrice(rs.getInt("price"));
//	item.setCreater_id(rs.getInt("category_id"));
//	item.setFile_name1(rs.getString("file_name1"));
//	item.setFile_name2(rs.getString("file_name2"));
//	item.setFile_name3(rs.getString("file_name3"));
//	item.setNumber(rs.getInt("number"));
//	item.setCreatedate(rs.getDate("create_date"));
//
//
//		itemList.add(item);
//
//}
//
//	for (int i=0; i<itemList.size(); i++) {
//
//		for(int j = 0 ; j<itemList.size(); j++) {
//
//			if(itemList.get(i).getId() == itemList.get(j).getId()) {
//				System.out.println(itemList.get(i).getId() +"　と　"+ itemList.get(j).getId());
//				itemList.remove(itemList.get(j));
//			}
//		}
//	}
//for (int i=0; i<itemList.size(); i++) {
//
//		for(int j = itemList.size()-1; j>=0; j--) {
//
//			if(itemList.get(i).getId() == itemList.get(j).getId()) {
//				System.out.println(itemList.get(i).getId() +"　と　"+ itemList.get(j).getId());
//				itemList.remove(itemList.get(j));
//
//			}
//		}
//	}
//

//while (rs.next()) {
//
//	ItemBeans item = new ItemBeans();
//	int id =rs.getInt("item_id");
//
//	item.setId(rs.getInt("item_id"));
//	item.setCreater_id(rs.getInt("creater_id"));
//	item.setItem_name(rs.getString("item_name"));
//	item.setDetail(rs.getString("detail"));
//	item.setPrice(rs.getInt("price"));
//	item.setCreater_id(rs.getInt("category_id"));
//	item.setFile_name1(rs.getString("file_name1"));
//	item.setFile_name2(rs.getString("file_name2"));
//	item.setFile_name3(rs.getString("file_name3"));
//	item.setNumber(rs.getInt("number"));
//	item.setCreatedate(rs.getDate("create_date"));
//
//	if(itemList.size() == 0) {
//		itemList.add(item);
//	}else {
//
//	for(int i = 0; i<itemList.size(); i++) {
//		if(itemList.get(i).getId()!=id) {
//			System.out.println(itemList.get(i).getId() +"　と　"+ item.getId());
//		itemList.add(item);
//		break;
//		}
//
//	}
//
//}
//
//}
//






//
//
//
//while (rs.next()) {
//
//	ItemBeans item = new ItemBeans();
//
//	item.setId(rs.getInt("item_id"));
//	item.setCreater_id(rs.getInt("creater_id"));
//	item.setItem_name(rs.getString("item_name"));
//	item.setDetail(rs.getString("detail"));
//	item.setPrice(rs.getInt("price"));
//	item.setCreater_id(rs.getInt("category_id"));
//	item.setFile_name1(rs.getString("file_name1"));
//	item.setFile_name2(rs.getString("file_name2"));
//	item.setFile_name3(rs.getString("file_name3"));
//	item.setNumber(rs.getInt("number"));
//	item.setCreatedate(rs.getDate("create_date"));
//
//	//アイテムリストにいったんいれる
//	itemList.add(item);
//
//}
//
//
//for (int i = itemList.size()-1; i>=0; i--) {
//
//		for(int j=0; j<itemList.size(); j++) {
//
//			if(itemList.get(i).getId() == itemList.get(j).getId()) {
//				System.out.println(itemList.get(i).getId() +"　と　"+ itemList.get(j).getId());
//				itemList.remove(itemList.get(i));
//
//			}
//		}
//	}





//
//while (rs.next()) {
//
//	ItemBeans item = new ItemBeans();
//	int id =rs.getInt("item_id");
//
//	item.setId(rs.getInt("item_id"));
//	item.setCreater_id(rs.getInt("creater_id"));
//	item.setItem_name(rs.getString("item_name"));
//	item.setDetail(rs.getString("detail"));
//	item.setPrice(rs.getInt("price"));
//	item.setCreater_id(rs.getInt("category_id"));
//	item.setFile_name1(rs.getString("file_name1"));
//	item.setFile_name2(rs.getString("file_name2"));
//	item.setFile_name3(rs.getString("file_name3"));
//	item.setNumber(rs.getInt("number"));
//	item.setCreatedate(rs.getDate("create_date"));
//
//	if(itemList.size() == 0) {
//		itemList.add(item);
//	}else {
//
//	for(int i = itemList.size()-1; i>=0; i--) {
//		if(itemList.get(i).getId()!=id) {
//			System.out.println(itemList.get(i).getId() +"　と　"+ item.getId());
//		itemList.add(item);
//		}
//
//	}
//
//}
//
//}
//


