package dao;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import beans.ItemBeans;

public class MyWebSiteHelper {

	/**
	 * セッションから指定データを取得（入力した内容の削除も一緒に行う）
	 */

	//サインアップコンファームのサーブレットでエラーが出るその場合サインアップに戻る　そして、以下のメッソドでエラーメッセージを残し、入力した内容セッションを消す
	public static Object cutSessionAttribute(HttpSession session, String str) {

		//いったん受け取り変数を作る
		Object test = session.getAttribute(str);
		//それを消す
		session.removeAttribute(str);

		//上の変数に入れて返す
		return test;
	}

	/**
	 * ログインIDのバリデーション
	 * 0-9a-zA-Z-_ ログインidに入れられる
	 *
	 * @param inputLoginId
	 * @return
	 */
	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}

		//上記以外がにゅりょっくされた場合　false が返り、エラーを起こさせる
		return false;

	}

	/*
	 * 各アイテムの小計を出していく
	 * そしてそれをセットしていく
	 * */

	public static void getItemTotal(ArrayList<ItemBeans> cart) {
		// TODO 自動生成されたメソッド・スタブ

		for (int i = 0; i < cart.size(); i++) {
			cart.get(i).setTotalPrice(cart.get(i).getBuyNumber() * cart.get(i).getPrice());
		}

	}

	/**
	 * 商品の合計購入点数を算出する
	 *
	 */
	public static int getTotalItemNumber(ArrayList<ItemBeans> items) {
		int total = 0;
		for (int i = 0; i < items.size(); i++) {
			total += items.get(i).getBuyNumber();
		}
		return total;
	}

	/**
	 * 商品の合計金額を算出する
	 *
	 */
	public static int getTotalItemPrice(ArrayList<ItemBeans> items) {
		int total = 0;
		for (int i = 0; i < items.size(); i++) {
			total += items.get(i).getTotalPrice();
		}
		return total;
	}

}
