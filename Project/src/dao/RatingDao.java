package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.RatingBeans;

public class RatingDao {

	/*
	 * 商品入力
	 * */

	public static void SendSore(int item_id, int score, String coment, int UserId) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"INSERT INTO `rating` (`item_id`, `score`,`coment`,`create_date`,`userid` ) VALUES (?,?,?,now(),?)");

			st.setInt(1, item_id);
			st.setInt(2, score);
			st.setString(3, coment);
			st.setInt(4, UserId);

			st.executeUpdate();

			System.out.println("Rating has been completed");

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * 購入詳細の欄で渡ったスコアを
	 * 表示する
	 * */

	public static ArrayList<RatingBeans> getRateingByItemId(int item_id) throws SQLException {

		Connection con = null;
		PreparedStatement st = null;

		try {
			//DBとの接続
			con = DBManager.getConnection();

			//実行したいSQLを入れる　要確認
			st = con.prepareStatement(
					"SELECT * FROM `rating` JOIN `t_user`  ON t_user.id = rating.userid  where `item_id` = ?");
			st.setInt(1, item_id);

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<RatingBeans> rateList = new ArrayList<RatingBeans>();

			while (rs.next()) {

				RatingBeans rate = new RatingBeans();

				rate.setId(rs.getInt("id"));
				rate.setItem_id(rs.getInt("item_id"));
				rate.setScore(rs.getInt("score"));
				rate.setComent(rs.getString("coment"));
				rate.setCreate_date(rs.getDate("create_date"));
				rate.setUserid(rs.getInt("userid"));
				rate.setUserIcon(rs.getString("user_icon"));

				rateList.add(rate);
			}

			System.out.println("getAllItem completed");

			return rateList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
