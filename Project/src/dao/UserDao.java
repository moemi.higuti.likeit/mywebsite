package dao;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserBeans;

public class UserDao {

	//暗号か　メソッド
	public static String code(String password1) {

		String source = password1;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(StandardCharsets.UTF_8));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		return result;
	}

	//新規登録画面t
	public static void userInsert(String name, String address1, String address2, String login_password, String login_id,
			String user_icon, String inputFirstName) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			st = con.prepareStatement(
					"INSERT INTO `t_user` ( name, address1, address2, login_id, login_password, user_icon,create_date,first_name) VALUES(?,?,?,?,?,?,NOW(),?)");

			st.setString(1, name);
			st.setString(2, address1);
			st.setString(3, address2);
			st.setString(4, login_id);
			st.setString(5, UserDao.code(login_password));
			st.setString(6, user_icon);
			st.setString(7, inputFirstName);

			st.executeUpdate();

			System.out.println("registaring has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * ログインIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */

	public static UserBeans getUserInfoByLoginId(String logid) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM `t_user` where login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement st = conn.prepareStatement(sql);

			st.setString(1, logid);

			ResultSet rs = st.executeQuery();

			// 結果表に格納されたレコードの内容を

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String name = rs.getString("name");
			String address1 = rs.getString("address1");
			String address2 = rs.getString("address2");
			String login_id = rs.getString("login_id");
			String login_password = rs.getString("login_password");
			String user_icon = rs.getString("user_icon");
			Date create_date = rs.getDate("create_date");
			String first_name = rs.getString("first_name");

			return new UserBeans(id, name, address1, address2, login_id, login_password, user_icon, create_date,
					first_name);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * ログインIDからユーザー情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */

	public static UserBeans getUserInfoById(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM `t_user` where id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement st = conn.prepareStatement(sql);

			st.setString(1, id);

			ResultSet rs = st.executeQuery();

			// 結果表に格納されたレコードの内容を

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String name = rs.getString("name");
			String address1 = rs.getString("address1");
			String address2 = rs.getString("address2");
			String login_id = rs.getString("login_id");
			String login_password = rs.getString("login_password");
			String user_icon = rs.getString("user_icon");
			Date create_date = rs.getDate("create_date");
			String first_name = rs.getString("first_name");

			return new UserBeans(id1, name, address1, address2, login_id, login_password, user_icon, create_date,
					first_name);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * loginIdの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	/**
	 * ログイン時の情報を取得する
	 *
	 * @param useId
	 *            ユーザーID
	 * @return udbList 引数から受け取った値に対応するデータを格納する
	 * @throws SQLException
	 *             呼び出し元にcatchさせるためスロー
	 */

	public static UserBeans getUserInfoWhenLogin(String logid, String password) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM `t_user` WHERE login_id = ? and login_password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement st = conn.prepareStatement(sql);

			st.setString(1, logid);
			st.setString(2, password);

			ResultSet rs = st.executeQuery();

			// 結果表に格納されたレコードの内容を

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			int id = rs.getInt("id");
			String name = rs.getString("name");
			String address1 = rs.getString("address1");
			String address2 = rs.getString("address2");
			String login_id = rs.getString("login_id");
			String login_password = rs.getString("login_password");
			String user_icon = rs.getString("user_icon");
			Date create_date = rs.getDate("create_date");
			String first_name = rs.getString("first_name");

			return new UserBeans(id, name, address1, address2, login_id, login_password, user_icon, create_date,
					first_name);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public static void UpDate(String name, String address1, String address2, String login_id,
			String login_password, String user_icon, String first_name, String id) {

		Connection con = null;
		PreparedStatement st = null;

		try {
			// データベース接続
			con = DBManager.getConnection();

			// 実行SQL文字列定義
			st = con.prepareStatement(
					"UPDATE `t_user` SET  name=?, address1 =?,address2=?, login_id= ?, login_password =?, user_icon=?, first_name =? WHERE id = ?");

			st.setString(1, name);
			st.setString(2, address1);
			st.setString(3, address2);
			st.setString(4, login_id);
			st.setString(5, UserDao.code(login_password));
			st.setString(6, user_icon);
			st.setString(7, first_name);
			st.setString(8, id);

			st.executeUpdate();

			System.out.println("updating has been completed");

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {

				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * クリーえーたーIDでそのユーザー情報を取得
	 * */

	public static UserBeans getItemCreaterID(int creater_id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM  `t_user` JOIN `t-item` ON `t-item`.creater_id = `t_user`.id where `t-item`.creater_id = ?");
			st.setInt(1, creater_id);

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			UserBeans creater = new UserBeans();

			if (rs.next()) {

				creater.setName(rs.getString("name"));
				creater.setFirst_name(rs.getString("first_name"));
				creater.setAddress1(rs.getString("address1"));
				creater.setAddress2(rs.getString("address2"));
				creater.setLogin_id(rs.getString("login_id"));
				creater.setLogin_password(rs.getString("login_password"));
				creater.setUser_icon(rs.getString("user_icon"));
				creater.setCreate_date(rs.getDate("create_date"));

			}

			System.out.println("searching userInfo by CreaterID has been completed");

			return creater;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/*
	 * クリーえーたーIDでそのユーザー情報を取得
	 * */

	public static ArrayList<UserBeans> getItemCreaterIdwhenshowUpart(int creater_id) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT * FROM  `t_user` JOIN `t-item` ON `t-item`.creater_id = `t_user`.id where `t-item`.creater_id = ?");
			st.setInt(1, creater_id);

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<UserBeans> createrList = new ArrayList<UserBeans>();

			while (rs.next()) {

				UserBeans creater = new UserBeans();

				creater.setId(rs.getInt("id"));
				creater.setName(rs.getString("name"));
				creater.setFirst_name(rs.getString("first_name"));
				creater.setAddress1(rs.getString("address1"));
				creater.setAddress2(rs.getString("address2"));
				creater.setLogin_id(rs.getString("login_id"));
				creater.setLogin_password(rs.getString("login_password"));
				creater.setUser_icon(rs.getString("user_icon"));
				creater.setCreate_date(rs.getDate("create_date"));

				createrList.add(creater);
			}

			System.out.println("searching userInfo by CreaterID has been completed");

			return createrList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public static ArrayList<UserBeans> getFindAll() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("select * from `t_user` ");

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<UserBeans> userList = new ArrayList<UserBeans>();

			while (rs.next()) {

				UserBeans user = new UserBeans();

				user.setId(rs.getInt("id"));
				user.setName(rs.getString("name"));
				user.setFirst_name(rs.getString("first_name"));
				user.setAddress1(rs.getString("address1"));
				user.setAddress2(rs.getString("address2"));
				user.setLogin_id(rs.getString("login_id"));
				user.setLogin_password(rs.getString("login_password"));
				user.setUser_icon(rs.getString("user_icon"));
				user.setCreate_date(rs.getDate("create_date"));

				userList.add(user);
			}

			System.out.println("searching userInfo by CreaterID has been completed");

			return userList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * idをもとにそのゆーざーidを消す
	 *
	 */
	public static void Delete(String id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String sql = "delete from `t_user` where id =?";

			// SQLの?パラメータに値を設定
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 購入情報のあるユーザ情報を取得する
	 * @return
	 */
	public static ArrayList<UserBeans> getFindAllBuier() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT distinct `t_user`.id,`t_user`.login_id,`t_user`.user_icon  FROM `t_user` JOIN `t_buy` ON `t_buy`.user_id = `t_user`.id");

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<UserBeans> userList = new ArrayList<UserBeans>();

			while (rs.next()) {

				UserBeans user = new UserBeans();

				user.setId(rs.getInt("id"));
				user.setLogin_id(rs.getString("login_id"));
				user.setUser_icon(rs.getString("user_icon"));

				userList.add(user);
			}

			System.out.println("searching userInfo by CreaterID has been completed");

			return userList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	/**
	 * 販売履歴のあるユーザ情報を取得する
	 * @return
	 */
	public static ArrayList<UserBeans> getFindAllSeller() throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement(
					"SELECT distinct `t_user`.id,`t_user`.login_id,`t_user`.user_icon FROM `t-item` JOIN `t_user` ON `t-item`.creater_id = `t_user`.id");

			ResultSet rs = st.executeQuery();

			//戻り値がリスト型なので、リストのインスタンスを作る
			ArrayList<UserBeans> userList = new ArrayList<UserBeans>();

			while (rs.next()) {

				UserBeans user = new UserBeans();

				user.setId(rs.getInt("id"));
				user.setLogin_id(rs.getString("login_id"));
				user.setUser_icon(rs.getString("user_icon"));

				userList.add(user);
			}

			System.out.println("searching sellerInfo has been completed");

			return userList;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

}
