package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import dao.CategoryDao;
import dao.ItemDao;

/**
 * Servlet implementation class Index
 */
@WebServlet("/Index")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Index() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		try {

			//商品情報を取得　おすすめの四つをランダムでカテゴリー別　商品別に表示
			ArrayList<CategoryBeans> categoryList = CategoryDao.getCategory();
			ArrayList<ItemBeans> itemList = ItemDao.getRandItemShow(4);


			//リクエストスコープに上のやつをセット
			request.setAttribute("itemList", itemList);
			request.setAttribute("categoryList", categoryList);

			//セッションにsearchWordが入っていたらもらってくる
			String searchWord = (String)session.getAttribute("searchWord");

			//それを破棄する
			if(searchWord != null) {
				session.removeAttribute("searchWord");
			}

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}


}


}
