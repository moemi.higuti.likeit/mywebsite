package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDelete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		try {
			//UserBuyFinallyConfirmからそれ以前から選択されている配送のIDとデリートするアイテムのIDを入手する
			String s = request.getParameter("delivery_method_id");
			String deleteItemId = request.getParameter("delete");

			//カートのアレイリストを入手
			ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

			if (deleteItemId != null) {
				//削除対象の商品を削除
				//消したいもののIDだけが入っているので、そのIDとカートのIDを照らし合わせる

				for (ItemBeans cartInItem : cart) {
					//カートに入っているアイテムのIDとデリートしたいIDをfor文で回しながら見つける

					if (cartInItem.getId() == Integer.parseInt(deleteItemId)) {
						cart.remove(cartInItem);
						break;
					}
				}

			}
			
			//メッセージとリクエストで、そして、メソッドIDをもってリダイレクション
			request.setAttribute("cartActionMessage", "削除しました");
			response.sendRedirect("UserBuyFinallyConfirm?delivery_method_id=" + s);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
