package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.CategoryBeans;
import beans.ItemBeans;
import beans.RatingBeans;
import beans.UserBeans;
import dao.CategoryDao;
import dao.ItemDao;
import dao.RatingDao;
import dao.UserDao;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/ItemDetail")
public class ItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Object item;
	private Object cate;
	private Object category_id;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//選択された商品のIDを型変換し利用
		int id = Integer.parseInt(request.getParameter("item_id"));

		try {

			//itemidを取得してそのアイテムの情報を取得する
			ItemBeans item = ItemDao.getItemByItemID(id);

			//itemのカテゴリーIdでカテゴリー名を取得　そして変数に入れる
			CategoryBeans cate = CategoryDao.getCategoryByID(item.getCategory_id());

			//アイテム情報だけでなくクリエーターIDでクリエーターの情報も持ってくる
			UserBeans creater = UserDao.getItemCreaterID(item.getCreater_id());

			ArrayList<RatingBeans> rate = RatingDao.getRateingByItemId(id);

			//リクエストパラメーターにセット
			request.setAttribute("cate", cate);
			request.setAttribute("creater", creater);
			request.setAttribute("item", item);
			request.setAttribute("rate", rate);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("/WEB-INF/jsp/item_detail.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
