package mywebsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class ItemInsertResult
 */
@WebServlet("/ItemInsertResult")
public class ItemInsertResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemInsertResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする 文字化け対策
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		try {
			// URLからGETパラメータとしてIDを受け取る
			String id = request.getParameter("id");

			// 確認用：idをコンソールに出力
			System.out.println(id);

			//idでユーザー情報を引き出す
			UserBeans user = UserDao.getUserInfoById(id);

			//jspに入力されたものをゲットして、それぞれの変数に入れていく
			int itemCreaterId = Integer.parseInt(request.getParameter("hidden0"));
			String itemName = request.getParameter("hidden1");
			String itemDetail = request.getParameter("hidden2");
			int itemPrice = Integer.parseInt(request.getParameter("hidden3"));
			int itemCate = Integer.parseInt(request.getParameter("hidden4"));
			int itemNumber = Integer.parseInt(request.getParameter("hidden8"));
			String itemPic1 = request.getParameter("hidden5");
			String itemPic2 = request.getParameter("hidden6");
			String itemPic3 = request.getParameter("hidden7");

			//上記に作ったインスタンスに情報を入れていく
			ItemBeans itemInsert = new ItemBeans();

			//セッターを用いて、インスタンスに入れていく
			itemInsert.setCreater_id(itemCreaterId);
			itemInsert.setItem_name(itemName);
			itemInsert.setDetail(itemDetail);
			itemInsert.setPrice(itemPrice);
			itemInsert.setCategory_id(itemCate);
			itemInsert.setNumber(itemNumber);
			itemInsert.setFile_name1(itemPic1);
			itemInsert.setFile_name2(itemPic2);
			itemInsert.setFile_name3(itemPic3);

			//購入したいアイテム情報をデーターベースにいれていく
			ItemDao.InsertItem(itemInsert);

			//リクエストスコープに上のメソッドをセット
			request.setAttribute("user", user);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/iteminsertresult.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
