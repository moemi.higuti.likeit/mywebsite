package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemSearchByrating
 */
@WebServlet("/ItemSearchByrating")
public class ItemSearchByrating extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemSearchByrating() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//1ページに表示する商品数
		final int PAGE_MAX_ITEM_COUNT = 8;

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		try {

			//選択されたカテゴリー名を取得
			String score = request.getParameter("score");

			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer.parseInt(request.getParameter("page_num") == null
					? "1"
					: request.getParameter("page_num"));

			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("searchWord", score);

			// カテゴリーから出た商品リストを取得 ページ表示分のみ
			ArrayList<ItemBeans> searchResultItemListByCategoryName = ItemDao.getItemsByRating(score,
					pageNum, PAGE_MAX_ITEM_COUNT);

			// 検索ワードに対しての総ページ数を取得
			double itemCount = ItemDao.getItemCountByRating(score);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);

			// 総ページ数
			request.setAttribute("pageMax", pageMax);

			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", searchResultItemListByCategoryName);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行くitem_serch_result
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/category.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
