package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemSearchResult
 */
@WebServlet("/ItemSearchResult")
public class ItemSearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemSearchResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//1ページに表示する商品数
		final int PAGE_MAX_ITEM_COUNT = 8;

		try {

			//検索欄から検索する文字を入手する
			String searchWord = request.getParameter("search_word");


			//表示ページ番号 未指定の場合 1ページ目を表示
			int pageNum = Integer
					.parseInt(request.getParameter("page_num") == null ? "1" : request.getParameter("page_num"));


			// 商品リストを取得 ページ表示分のみ
			ArrayList<ItemBeans> searchResultItemList = ItemDao.getItemsByItemName(searchWord, pageNum,
					PAGE_MAX_ITEM_COUNT);


			// 検索ワードに対しての総ページ数を取得
			double itemCount = ItemDao.getItemCount(searchWord);
			int pageMax = (int) Math.ceil(itemCount / PAGE_MAX_ITEM_COUNT);

			//総アイテム数
			request.setAttribute("itemCount", (int) itemCount);

			// 総ページ数
			request.setAttribute("pageMax", pageMax);

			// 表示ページ
			request.setAttribute("pageNum", pageNum);
			request.setAttribute("itemList", searchResultItemList);

			// 新たに検索されたキーワードをセッションに格納する
			session.setAttribute("searchWord", searchWord);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行くitem_serch_result
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/item_serch_result.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
