package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;

/**
 * Servlet implementation class LogOut
 */
@WebServlet("/LogOut")
public class LogOut extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogOut() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//ユーザー情報、カートの情報を取得する
		UserBeans user = (UserBeans) session.getAttribute("user");
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		// ユーザ一覧のサーブレットにリダイレクト
		//上で取得した情報をもってUSERLISTに行く
		if (user == null) {
			response.sendRedirect("Index");
		}

		// ログイン時に保存したセッション内のユーザ情報を削除
		//入力した自分の個人情報をがめんから消す
		session.setAttribute("isLogin", false);
		session.removeAttribute("userId");
		session.removeAttribute("user");
		session.removeAttribute("cart");

		if (user == null) {
			System.out.println("logOut is done");
		}
		if (cart == null) {
			System.out.println("logOut is done");
		}

		// ログインjspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/logout.jsp");
		dispatcher.forward(request, response);
	}

}
