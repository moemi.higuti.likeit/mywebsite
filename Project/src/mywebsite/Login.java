package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.CategoryDao;
import dao.ItemDao;
import dao.MyWebSiteHelper;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//ログイン失敗時に使用するため　もしも失敗したら。入力した情報だけをもってここに戻ってくる
		String isLogin = session.getAttribute("loginId") != null
				? (String) MyWebSiteHelper.cutSessionAttribute(session, "loginId")
				: "";

		//ログイン情報で失敗した時の処理　このページに飛ぶまえの jsp名が入っている　そして、次そこに飛ぶ
		String loginErrorMessage = (String) MyWebSiteHelper.cutSessionAttribute(session, "loginErrorMessage");

		//リクエストでセットする
		request.setAttribute("isLogin", isLogin);
		request.setAttribute("loginErrorMessage", loginErrorMessage);
		request.setAttribute("loginActionMessage", "ログインしてください");


		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginId");
		String password1 = request.getParameter("password");

		//取得したパスワードを暗号化する
		String password = UserDao.code(password1);

		//上記で取得した情報でログインする人の情報を取得する
		UserBeans user = UserDao.getUserInfoWhenLogin(loginId, password);

		//もしもログイン失敗　user情報が見つけられなかったら
		if (user == null) {

			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "ログインに失敗しました");

			//カテゴリー、アイテムのリストのインスタンスを作る
			ArrayList<CategoryBeans> categoryList = null;
			ArrayList<ItemBeans> itemList = null;

			try {

				//インデックスページに飛ぶためその時の　必要な情報を入手
				categoryList = CategoryDao.getCategory();
				itemList = ItemDao.getRandItemShow(4);


				//リクエストスコープに上のメソッドをセット
				request.setAttribute("itemList", itemList);
				request.setAttribute("categoryList", categoryList);

				//セッションにsearchWordが入っていたらもらってくる
				String searchWord = (String) session.getAttribute("searchWord");

				//それを破棄する
				if (searchWord != null) {
					session.removeAttribute("searchWord");
				}

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);
				return;


			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}




	}/** テーブルに該当のデータが見つかった場合 **/
		session.setAttribute("user", user);
		session.setAttribute("userInfo", user);
		session.setAttribute("isLogin", true);


		//もしも管理者がログインした場合はマネージャーサーブレットに飛ぶ
		if (user.getId() == 1) {

			//上で取得した情報をもってjspに行く
			response.sendRedirect("Maneger");

			return;

		}

		//上で取得した情報をもってjspに行く
		response.sendRedirect("User");

	}

}
