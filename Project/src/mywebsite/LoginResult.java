package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.MyWebSiteHelper;
import dao.UserDao;

/**
 * Servlet implementation class LoginResult
 */
@WebServlet("/LoginResult")
public class LoginResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//ログイン失敗時に使用するため
		String inputLoginId = session.getAttribute("loginId") != null
				? (String) MyWebSiteHelper.cutSessionAttribute(session, "loginId")
				: "";

		//ログインで失敗したときに失敗したときのJsp名を持ってくる
		String loginErrorMessage = (String) MyWebSiteHelper.cutSessionAttribute(session, "loginErrorMessage");

		request.setAttribute("inputLoginId", inputLoginId);
		request.setAttribute("loginErrorMessage", loginErrorMessage);
		request.setAttribute("loginActionMessage", "ログインしてください");

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {
			//パラメーターから取得
			String loginId = request.getParameter("loginId");
			String password1 = request.getParameter("password");

			String password = UserDao.code(password1);

			UserBeans user = UserDao.getUserInfoWhenLogin(loginId, password);

			//ユーザーIDが取得できたなら
			if (user != null) {
				session.setAttribute("isLogin", true);
				session.setAttribute("loginId", loginId);

				session.setAttribute("user", user);

				//ログイン前のページを取得
				String returnStrUrl = (String) MyWebSiteHelper.cutSessionAttribute(session, "returnStrUrl");

				//ログイン前ページにリダイレクト。指定がない場合Index
				response.sendRedirect(returnStrUrl != null
						? returnStrUrl
						: "Index");
			} else {
				session.setAttribute("loginId", loginId);
				session.setAttribute("loginErrorMessage", "入力内容が正しくありません");
				response.sendRedirect("Login");
			}
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
