package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class Maneger
 */
@WebServlet("/Maneger")
public class Maneger extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Maneger() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		try {
			//全ユーザー情報を取得する
			ArrayList<UserBeans> userList = UserDao.getFindAll();

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("userList", userList);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/manager.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		// なんのボタンが押されたかの情報を取得する
		String confirmed = request.getParameter("confirm_button");

		try {
			switch (confirmed) {

			//購入履歴のあるユーザーを調べる
			case "buier":

				//購入履歴のある人を調べる
				 ArrayList<UserBeans> buierrList = UserDao.getFindAllBuier();

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", buierrList);
				break;

			case "seller":

				//販売履歴のある人を調べる
				 ArrayList<UserBeans> sellerList = UserDao.getFindAllSeller();

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", sellerList);

				break;

			case "allUser":

				//全員抽出のメソッド
				ArrayList<UserBeans> userList = UserDao.getFindAll();

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userList);
				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				break;
			}

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/manager.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}
