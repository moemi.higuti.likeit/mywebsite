package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.MyWebSiteHelper;
import dao.UserDao;

/**
 * Servlet implementation class SignUpConfirm
 */
@WebServlet("/SignUpConfirm")
public class SignUpConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignUpConfirm() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け用
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

			//jspに入力されたものをゲットして、変数に入れていく
			String inputName = request.getParameter("lastname");
			String inputAddress = request.getParameter("inputAddress");
			String inputAddress2 = request.getParameter("inputAddress2");
			String inputPassword = request.getParameter("inputPassword");
			String inputPasswordCon = request.getParameter("inputPasswordCon");
			String inputLoginId = request.getParameter("loginId");
			String inputPic = request.getParameter("pic");
			String inputFirstName = request.getParameter("firstname");

			//インスタンスを作る
			UserBeans udb = new UserBeans();

			//上記の変数をインスタンスで作ったものをセットしていく
			udb.setName(inputName);
			udb.setAddress1(inputAddress);
			udb.setAddress2(inputAddress2);
			udb.setLogin_password(inputPassword);
			udb.setLogin_id(inputLoginId);
			udb.setUser_icon(inputPic);
			udb.setFirst_name(inputFirstName);

			//メッセージを入れる変数を作る
			String validationMessage = "";


			// 入力されているパスワードが確認用と等しいか
			if (!inputPassword.equals(inputPasswordCon)) {
				validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
			}
			// パスワードが、6以上か
			if (inputPassword.length() < 5) {
				validationMessage += "パスワードが短すぎます<br>";
			}

			// ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
			if (!MyWebSiteHelper.isLoginIdValidation(udb.getLogin_id())) {
				validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます<br>";
			}
			// loginIdの重複をチェック
			if (UserDao.isOverlapLoginId(udb.getLogin_id(), 0)) {
				validationMessage += "ほかのユーザーが使用中のログインIDです";
			}

			// バリデーションエラーメッセージがないなら確認画面へ
			if (validationMessage.length() == 0) {

				request.setAttribute("udb", udb);

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				//もしも、エラーメッセージが一つでもあれば、入力した情報をもっていく
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign_up_confirm.jsp");
				dispatcher.forward(request, response);

			} else {

				//全ての情報をせっとしていく
				session.setAttribute("udb", udb);
				session.setAttribute("validationMessage", validationMessage);

				//リダイレクションする
				response.sendRedirect("SingUp");
			}

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
