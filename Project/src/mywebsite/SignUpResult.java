package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class SignUpResult
 */
@WebServlet("/SignUpResult")
public class SignUpResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SignUpResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//jspに入力されたものをゲットして、変数に入れていく
		String inputName = request.getParameter("hidden3");
		String inputAddress = request.getParameter("hidden5");
		String inputAddress2 = request.getParameter("hidden6");
		String inputPassword = request.getParameter("hidden7");
		String inputLoginId = request.getParameter("hidden2");
		String inputPic = request.getParameter("hidden1");

		String inputFirstName = request.getParameter("hidden4");

		//入力された情報をもってユーザー情報を入れていく
		UserDao.userInsert(inputName, inputAddress, inputAddress2, inputPassword, inputLoginId, inputPic,
				inputFirstName);

		//ユーザー情報を調べる
		UserBeans user = UserDao.getUserInfoByLoginId(inputLoginId);

		//ユーザー情報をセットしていく
		request.setAttribute("user", user);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/signup_result.jsp");
		dispatcher.forward(request, response);

	}

}
