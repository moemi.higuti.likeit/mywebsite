package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.MyWebSiteHelper;

/**
 * Servlet implementation class SingUp
 */
@WebServlet("/SingUp")
public class SingUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SingUp() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//Userbeans のインスタンスを作りかの条件式
		UserBeans udb =	session.getAttribute("udb") != null

			//上の場合はセッション次に進んでいたのでエラーで戻ってきたもの
			//中を消して、エラーメッセージだけを出させる
			? (UserBeans) MyWebSiteHelper.cutSessionAttribute(session, "udb")

			//本当に初めてこのページに来たのでインスタントを作る
			: new UserBeans();

		//メッセージを受け取る変数を作る
		String validationMessage = (String) MyWebSiteHelper.cutSessionAttribute(session, "validationMessage");


		//もし次のページから戻ってきていたなら　消したセッションをセット　でエラーメッセージを表示
		request.setAttribute("udb", udb);
		request.setAttribute("validationMessage", validationMessage);


		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign_up.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け用
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		try {

			//jspに入力されたものをゲットして、変数に入れていく
			String inputName = request.getParameter("hidden3");
			String inputAddress = request.getParameter("hidden5");
			String inputAddress2 = request.getParameter("hidden6");
			String inputPassword = request.getParameter("hidden7");
			String inputLoginId = request.getParameter("hidden2");
			String inputPic = request.getParameter("hidden1");
			String inputFirstName = request.getParameter("hidden4");


			UserBeans udb = new UserBeans();


			udb.setName(inputName);
			udb.setAddress1(inputAddress);
			udb.setAddress2(inputAddress2);
			udb.setLogin_password(inputPassword);
			udb.setLogin_id(inputLoginId);
			udb.setUser_icon(inputPic);
			udb.setFirst_name(inputFirstName);

			request.setAttribute("udb", udb);


			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sign_up.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}
