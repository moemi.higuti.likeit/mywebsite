package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class User
 */
@WebServlet("/User")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public User() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		//ログインしているかどうか確かめるかの処理
		//サーブレットLoginでisLoginを持っているか確かめる
		Boolean isLogin = session.getAttribute("isLogin") != null
				//nullではなかったら持っている情報をゲットする

				? (Boolean) session.getAttribute("isLogin")

				//それ以外なら審議判定変数（isLogin）をFalseにする
				: false;

		//もし　変数　isLoginが塗る＝ログインされていない時の処理

		if (!isLogin) {
			// Sessionにリターンページ情報を書き込む　"UserCart"は戻りたいサーブレットの情報ログイン後に上のdogetに飛ぶ
			session.setAttribute("returnStrUrl", "User");

			//ログイン画面に行くときに下に記入したメッセージを記入したいのでセットしておく
			request.setAttribute("loginActionMessage", "ログインしてください");

			// Login画面にリダイレクト
			response.sendRedirect("Login");
			return;

		}

		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);

		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);


		try {
			ArrayList<CategoryBeans> categoryList = CategoryDao.getCategory();
			ArrayList<ItemBeans> itemList = ItemDao.getRandItemShow(4);

			//リクエストスコープに上のメソッドをセット
			request.setAttribute("itemList", itemList);

			request.setAttribute("categoryList", categoryList);

			//セッションにsearchWordが入っていたらもらってくる
			String searchWord = (String)session.getAttribute("searchWord");

			//それを破棄する
			if(searchWord != null) {
				session.removeAttribute("searchWord");
			}

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行くitem_serch_result

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
			dispatcher.forward(request, response);


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}





	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
