package mywebsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.RatingDao;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/UserBuiedDetail")
public class UserBuiedDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBuiedDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 *
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		// URLからGETパラメータとしてユーザーのIDを受け取る
		String UserId = request.getParameter("id");


		// URLからGETパラメータとしてIDを受け取る
		String ItemId = request.getParameter("item_id");
		System.out.print(ItemId);


		//リクエストスコープに上のメソッドをセット
		request.setAttribute("UserId", UserId);
		request.setAttribute("ItemId", ItemId);


		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行くitem_serch_result
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/User_buied_item_rating.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		//jspに入力されたものをゲットして、変数に入れていく
		//int型に直す
		int ItemId = Integer.parseInt(request.getParameter("item_id"));
		int UserId = Integer.parseInt(request.getParameter("user_id"));
		int Rate = Integer.parseInt(request.getParameter("rate"));
		String Comment = request.getParameter("comment");


		try {
			RatingDao.SendSore(ItemId, Rate, Comment, UserId);


			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/sent_coment.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}
}
