package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodBeans;
import beans.ItemBeans;
import dao.DeliveryMethodDao;
import dao.MyWebSiteHelper;

/**
 * Servlet implementation class UserBuyConfirm
 */
@WebServlet("/UserBuyConfirm")
public class UserBuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBuyConfirm() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// セッション
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//このサーブレットでカートの情報を使うのでカート情報を取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//foreach で回しているので、Values で全てを取得する　それをstring型の配列に入れていく
		String[] buyNumbers = request.getParameterValues("buyNumber");

		try {
			//カート画面に入力した購入数を取得
			for (int i = 0; i < cart.size(); i++) {
				//カートの数だけ先ほど取得したbuyNumbersをセットしていく
				cart.get(i).setBuyNumber(Integer.parseInt(buyNumbers[i]));
			}

			//カートの情報をもとにアイテムの総数を入手
			MyWebSiteHelper.getItemTotal(cart);


			// 購入確認画面で配送方法を選択するので取得する
			ArrayList<DeliveryMethodBeans> dmdbList = DeliveryMethodDao.getAllDeliveryMethodDataBeans();


			//もしもカートに情報がなかった時の処理
			if (cart.size() == 0) {

				request.setAttribute("cartActionMessage", "購入する商品がありません");


				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_cart.jsp");
				dispatcher.forward(request, response);
				return;
			}

			//配送方法の情報をセットする
			request.setAttribute("dmdbList", dmdbList);


			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userbuyconfirm.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
