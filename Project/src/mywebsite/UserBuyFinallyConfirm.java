package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.DeliveryMethodBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.DeliveryMethodDao;
import dao.MyWebSiteHelper;

/**
 * Servlet implementation class UserBuyFinallyConfirm
 */
@WebServlet("/UserBuyFinallyConfirm")
public class UserBuyFinallyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserBuyFinallyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//セッションがあったらセットする
    	HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		//選択された配送方法IDを取得
		int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));

		//選択されたIDをもとに配送方法Beansを取得
		DeliveryMethodBeans userSelectDMB = null;

		try {
			//前のページから配送のIdを取得して、それをもとに情報を変数に入れる
			userSelectDMB = DeliveryMethodDao.getDeliveryMethodByID(inputDeliveryMethodId);


			//買い物かご　新しく変数にカートの情報をカートに入れていく
			ArrayList<ItemBeans> cartIDBList = (ArrayList<ItemBeans>) session.getAttribute("cart");

			//そのカートの情報をもとに合計金額の算出と合計購入点数をだし、それぞれの変数に入れる
			int totalPrice = MyWebSiteHelper.getTotalItemPrice(cartIDBList);
			int totalBuyNumber = MyWebSiteHelper.getTotalItemNumber(cartIDBList);

			//BuyBeans　に新しいインスタンスを作る
			BuyBeans bdb = new BuyBeans();

			//上記で作成したインスタンスに各情報をセットしていく

			//ユーザーIdとBuyBeansのUser_idは同じなのでそれを用いる
			UserBeans user = (UserBeans)session.getAttribute("user");
			bdb.setUser_id(user.getId());

			bdb.setTotal_price(totalPrice);
			bdb.setDelivery_method_id(userSelectDMB.getId());

			//アイテムの合計額と、配送の合計額を出し、フォーマットも設定する
			int totalCost = bdb.getTotal_price() + userSelectDMB.getPrice();
			String Cost = String.format("%,d", totalCost);
			System.out.print(userSelectDMB.getId());


			//購入確定で利用
			session.setAttribute("Cost", Cost);
			session.setAttribute("totalBuyNumber", totalBuyNumber);
			session.setAttribute("userSelectDMB", userSelectDMB);
			session.setAttribute("bdb", bdb);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserBuyFinalConfirm.jsp");
			dispatcher.forward(request, response);


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}









    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		String s = request.getParameter("delivery_method_id");
		//選択された配送方法IDを取得
		int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));
		//選択されたIDをもとに配送方法Beansを取得
		DeliveryMethodBeans userSelectDMB = null;

		try {
			//前のページから配送のIdを取得して、それをもとに情報を変数に入れる
			userSelectDMB = DeliveryMethodDao.getDeliveryMethodByID(inputDeliveryMethodId);


			//買い物かご　新しく変数にカートの情報をカートに入れていく
			ArrayList<ItemBeans> cartIDBList = (ArrayList<ItemBeans>) session.getAttribute("cart");

			//そのカートの情報をもとに合計金額の算出と合計購入点数をだし、それぞれの変数に入れる
			int totalPrice = MyWebSiteHelper.getTotalItemPrice(cartIDBList);
			int totalBuyNumber = MyWebSiteHelper.getTotalItemNumber(cartIDBList);

			//BuyBeans　に新しいインスタンスを作る
			BuyBeans bdb = new BuyBeans();

			//上記で作成したインスタンスに各情報をセットしていく

			//ユーザーIdとBuyBeansのUser_idは同じなのでそれを用いる
			UserBeans user = (UserBeans)session.getAttribute("user");
			bdb.setUser_id(user.getId());

			bdb.setTotal_price(totalPrice);
			bdb.setDelivery_method_id(userSelectDMB.getId());

			//アイテムの合計額と、配送の合計額を出し、フォーマットも設定する
			int totalCost = bdb.getTotal_price() + userSelectDMB.getPrice();
			String Cost = String.format("%,d", totalCost);


			//購入確定で利用
			session.setAttribute("Cost", Cost);
			session.setAttribute("totalBuyNumber", totalBuyNumber);
			session.setAttribute("userSelectDMB", userSelectDMB);
			session.setAttribute("bdb", bdb);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserBuyFinalConfirm.jsp");
			dispatcher.forward(request, response);


		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}




	}

}
