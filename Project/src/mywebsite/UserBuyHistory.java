package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.DeliveryMethodBeans;
import beans.UserBeans;
import dao.BuyDao;
import dao.DeliveryMethodDao;
import dao.UserDao;

/**
 * Servlet implementation class UserBuyHistory
 */
@WebServlet("/UserBuyHistory")
public class UserBuyHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBuyHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);

		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);

		//idをもとに購入履歴を調べる
		List<BuyBeans> list = BuyDao.getBuyDataBeansByBuyIdList(id);

		//listの配送Iｄをもとに配送情報を取得する
		if (list.size() == 0) {

			request.setAttribute("InfoMessage", "購入履歴はありません");
		}

		for (int i = 0; i < list.size(); i++) {

			try {
				//購入履歴から配送方法の情報を取得する
				DeliveryMethodBeans DeliveryList = DeliveryMethodDao
						.getDeliveryMethodByID(list.get(i).getDelivery_method_id());

				request.setAttribute("DeliveryList", DeliveryList);
				request.setAttribute("list", list);

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行くitem_serch_result
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_buy_history.jsp");
				dispatcher.forward(request, response);

			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		}

	}

}
