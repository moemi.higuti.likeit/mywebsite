package mywebsite;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.ItemBeans;
import dao.BuyDao;
import dao.ItemDao;

/**
 * Servlet implementation class UserBuyList
 */
@WebServlet("/UserBuyList")
public class UserBuyList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBuyList() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		try {
			// URLからGETパラメータとしてその購入履歴のIDを受け取る
			String id = request.getParameter("buy_id");

			//購入履歴のリストのインスタンスを作る
			BuyDao buyDao = new BuyDao();

			//Idをもとに情報を入れていく
			BuyBeans bdb = buyDao.getBuyDataBeansByBuyIdNId(id);
			request.setAttribute("bdb", bdb);

			//アイテムの情報を取得していく
			List<ItemBeans> item = ItemDao.getCosNItem(id);
			request.setAttribute("item", item);

			//合計額の計算
			int totalPrice = (bdb.getTotal_price() + bdb.getDeliveryMethodPrice());

			//フォーマットをここで治す
			String total = String.format("%,d", totalPrice);

			//セットしていく
			request.setAttribute("total", total);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserBuylist.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}
}
