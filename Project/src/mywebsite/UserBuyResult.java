package mywebsite;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyBeans;
import beans.Buy_detailBeans;
import beans.ItemBeans;
import dao.BuyDao;
import dao.Buy_DetailDao;
import dao.ItemDao;
import dao.MyWebSiteHelper;

/**
 * Servlet implementation class UserBuyResult
 */
@WebServlet("/UserBuyResult")
public class UserBuyResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserBuyResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		try {
			// セッションからカート情報を取得　BuyBeans をこのページで使うので取得
			ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) MyWebSiteHelper.cutSessionAttribute(session, "cart");
			BuyBeans bdb = (BuyBeans) MyWebSiteHelper.cutSessionAttribute(session, "bdb");

			// 購入情報を登録　そして何種類購入したかとってくる
			int buyId = BuyDao.insertBuy(bdb);

			// 購入詳細情報を購入情報IDに紐づけして登録
			//cartの数は購入した種類の数とおなジ
			for (ItemBeans cartInItem : cart) {

				//購入したものの情報をBuy_detailBeansのビーンズに種類ずつ情報を入れていく
				Buy_detailBeans bddb = new Buy_detailBeans();

				bddb.setBuy_id(buyId);
				bddb.setItem_id(cartInItem.getId());
				bddb.setCategory_id(cartInItem.getCategory_id());
				bddb.setNumber(cartInItem.getBuyNumber());

				Buy_DetailDao.insertBuyDetail(bddb);
			}

			//購入した商品の数を取得して、元の個数と、買った数を減らす
			for (ItemBeans cartInItem : cart) {

				//購入したものの情報をBuy_detailBeansのビーンズに種類ずつ情報を入れていく
				ItemBeans rest = new ItemBeans();

				rest.setId(cartInItem.getId());
				rest.setNumber(cartInItem.getNumber());
				rest.setBuyNumber(cartInItem.getBuyNumber());

				ItemDao.getItemRestUpdate(rest);

			}

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user-buy_success.jsp");
			dispatcher.forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}

}
