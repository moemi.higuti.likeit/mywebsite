package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class UserCart
 */
@WebServlet("/UserCart")
public class UserCart extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserCart() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//カートを取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");

		//ログインしているかどうか確かめる
		Boolean isLogin = session.getAttribute("isLogin") != null
				? (Boolean) session.getAttribute("isLogin")
				: false;

		if (!isLogin) {

			//ログインされていなかったら　Jsp名をもって　ログイン画面に送る
			session.setAttribute("returnStrUrl", "UserCart");
			request.setAttribute("loginActionMessage", "ログインしてください");

			// Login画面にリダイレクト
			response.sendRedirect("Login");

		} else {

			//カートの情報をセットする
			session.setAttribute("cart", cart);
			request.setAttribute("cartActionMessage", "商品を追加しました");

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_cart.jsp");
			dispatcher.forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		//アイテムIDをurlから取得する
		int itemId = Integer.parseInt(request.getParameter("item_id"));

		//カートを取得
		ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cart");


		try {

			//ItemBeansのBeansに取得したIdをもとにアイテム情報を入れていく
			ItemBeans item = ItemDao.getItemByItemID(itemId);


			//カートがない場合カートのリストを作成する
			if (cart == null) {
				cart = new ArrayList<ItemBeans>();
			}

			//取得したアイテム情報をもとにそのアイテムの販売者の情報を取得
			UserBeans creater = UserDao.getItemCreaterID(item.getCreater_id());

			//上記で取得した情報を追加したbeansのフィールド内にアイテムの情報としてセットする
			//Joinですることも可だが、同じものがあったので、ただ、クリエーター情報をここで追加
			item.setCreaterName(creater.getLogin_id());
			item.setCreate_date(creater.getCreate_date());

			//カート内に同じ商品があるかどうかの処理  初期値として、審議判定の変数をFalseにセット
			boolean existFlg = false;

			//カート内の情報の文だけアイテムFor分を回す
			for (ItemBeans i : cart) {

				//変数 i　カートに入っていたアイテムIDと追加した分のアイテムIDを比べる
				//それと同じくアイテムの数と元から持っていた、買う数もそれ以上か比べる
				if (i.getId() == item.getId() && item.getNumber() <= i.getBuyNumber()){

						//　cart.add(item);はしない　また、BuyNumberも増やさないただメッセージを持たせる
						request.setAttribute("cartActionMessage", "購入個数をこれ以上増やせません");
						//変数をtureにする
						existFlg = true;

					}
				//ここで条件を入れないと、違う商品が入らない
				else if(i.getId() == item.getId()) {
						//同じであったらもとからセットしてあった数に１を追加する
						i.setBuyNumber(i.getBuyNumber() + 1);

						//　cart.add(item);はしない　カートに情報を入れたくないから
						request.setAttribute("cartActionMessage", "すでにその商品はカートの中にあります　購入数を追加しました");
						existFlg = true;
					}

				}


			//上記の処理以外＝新しくカートに入れる商品の処理
			if(!existFlg) {
				//アイテムの買う数を１入れる
				item.setBuyNumber(1);
				//リストカートにitemの情報を入れていく
				cart.add(item);
				request.setAttribute("cartActionMessage", "商品を追加しました");
			}

			//どの条件下でも取得したアイテム情報、配送情報、カート情報をもって飛ぶ
			//カート情報更新
			session.setAttribute("itemId", itemId);
			session.setAttribute("cart", cart);


			//ログインしているかどうか確かめるかの処理
			//サーブレットLoginでisLoginを持っているか確かめる
			Boolean isLogin = session.getAttribute("isLogin") != null
					//nullではなかったら持っている情報をゲットする

					? (Boolean) session.getAttribute("isLogin")

					//それ以外なら審議判定変数（isLogin）をFalseにする
					: false;

			//もし　変数　isLoginが塗る＝ログインされていない時の処理

			if (!isLogin) {
				// Sessionにリターンページ情報を書き込む　"UserCart"は戻りたいサーブレットの情報ログイン後に上のdogetに飛ぶ
				session.setAttribute("returnStrUrl", "UserCart");

				//ログイン画面に行くときに下に記入したメッセージを記入したいのでセットしておく
				request.setAttribute("loginActionMessage", "ログインしてください");

				// Login画面にリダイレクト
				response.sendRedirect("Login");

			} else if (cart.size() == 0) {
				//カートに何もない場合下のめーっせーじを持たせる
				request.setAttribute("cartErrorMessage", "購入する商品がありません");

			} else {

				//htmlでつっくった画面を出す
				// フォワード　サーブレットからJSPに行く
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_cart.jsp");
				dispatcher.forward(request, response);
			}
		}
	catch (SQLException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}



		}


}
