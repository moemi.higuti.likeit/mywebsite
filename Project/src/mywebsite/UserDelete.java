package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class UserDelete
 */
@WebServlet("/UserDelete")
public class UserDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDelete() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");


		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);


		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);

		try {
			//一覧の情報を　おすすめアイテムの情報を取得
			ArrayList<CategoryBeans> categoryList = CategoryDao.getCategory();
			ArrayList<ItemBeans> itemList = ItemDao.getRandItemShow(4);


			//リクエストスコープに上のメソッドをセット
			request.setAttribute("itemList", itemList);
			request.setAttribute("categoryList", categoryList);


			//セッションにsearchWordが入っていたらもらってくる
			String searchWord = (String) session.getAttribute("searchWord");


			//それを破棄する
			if (searchWord != null) {
				session.removeAttribute("searchWord");
			}

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行くitem_serch_result
			request.setAttribute("ManegerMessage", "こちらのユーザー情報を消していいですか？");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		//消したいユーザーのIdを取得
		String id = request.getParameter("id");
		UserDao.Delete(id);

		//マネージャー画面にリダイレクション
		response.sendRedirect("Maneger");

	}

}
