package mywebsite;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.MyWebSiteHelper;
import dao.UserDao;

/**
 * Servlet implementation class UserDetail
 */
@WebServlet("/UserDetail")
public class UserDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserDetail() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");


		//idをもとにユーザーの個人情報を取得する
		UserBeans user = UserDao.getUserInfoById(id);

		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		//jspに入力されたものをゲットして、変数に入れていく
		String inputName = request.getParameter("inputlastName");
		String inputAddress = request.getParameter("inputAddress");
		String inputAddress2 = request.getParameter("inputAddress2");
		String inputPassword = request.getParameter("inputPassword");
		String inputPasswordCon = request.getParameter("inputPassword2");
		String inputLoginId = request.getParameter("inputLoginId");
		String inputPic = request.getParameter("icon");
		String inputFirstName = request.getParameter("inputfirstName");

		UserBeans user = new UserBeans();

		user.setName(inputName);
		user.setAddress1(inputAddress);
		user.setAddress2(inputAddress2);
		user.setLogin_password(inputPassword);
		user.setLogin_id(inputLoginId);
		user.setUser_icon(inputPic);
		user.setFirst_name(inputFirstName);

		//メッセージ情報を入れる変数をつくる
		String validationMessage = "";


		// 入力されているパスワードが確認用と等しいか
		if (!inputPassword.equals("") || !inputPasswordCon.equals("")) {
			validationMessage += "まだパスワード入れないで<br>";
		}

		// ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
		if (!MyWebSiteHelper.isLoginIdValidation(user.getLogin_id())) {
			validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます<br>";
		}

		// loginIdの重複をチェック
		try {
			if (!user.getLogin_id().equals(inputLoginId) && UserDao.isOverlapLoginId(user.getLogin_id(), 0)) {
				validationMessage += "ほかのユーザーが使用中のログインIDです";
			}

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		// バリデーションエラーメッセージがないなら確認画面へ
		if (validationMessage.length() == 0) {

			UserDao.UpDate(inputName, inputAddress, inputAddress2, inputLoginId, inputPassword, inputPic,
					inputFirstName, id);

			//リクエストスコープに上のメソッドをセット
			UserBeans user1 = UserDao.getUserInfoById(id);

			request.setAttribute("user", user1);


			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行く
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_detail.jsp");
			dispatcher.forward(request, response);

		} else {
			session.setAttribute("user", user);
			session.setAttribute("validationMessage", validationMessage);
			response.sendRedirect("SingUp");
		}

	}

}
