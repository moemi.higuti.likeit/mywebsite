package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.CategoryDao;
import dao.UserDao;

/**
 * Servlet implementation class UserSellChanged
 */
@WebServlet("/UserSellChanged")
public class UserSellChanged extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSellChanged() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");


		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");


		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);


		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行くitem_serch_result
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_sell_changed.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け用
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// URLからGETパラメータとしてIDを受け取る
		//user.id　とクリエーターidは同じ
		String id = request.getParameter("id");

		int createrid = Integer.parseInt(request.getParameter("id"));
		//jspに入力されたものをゲットして、変数に入れていく
		String itemName = request.getParameter("itemName");
		String itemDetail = request.getParameter("detail");
		//int型に直す
		int itemPrice = Integer.parseInt(request.getParameter("price"));

		String itemCate = request.getParameter("category");


		int itemNumber = Integer.parseInt(request.getParameter("number"));
		String itemPic1 = request.getParameter("pic1");
		String itemPic2 = request.getParameter("pic2");
		String itemPic3 = request.getParameter("pic3");

		CategoryBeans cate = CategoryDao.getCategoryByName(itemCate);

		//上記に作ったインスタンスに情報を入れていく
		ItemBeans item = new ItemBeans();

		item.setCreater_id(createrid);
		item.setItem_name(itemName);
		item.setDetail(itemDetail);
		item.setPrice(itemPrice);
		item.setCategory_id(cate.getId());
		item.setNumber(itemNumber);
		item.setFile_name1(itemPic1);
		item.setFile_name2(itemPic2);
		item.setFile_name3(itemPic3);

		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);

		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);
		request.setAttribute("item", item);
		request.setAttribute("itemCate", itemCate);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_sell_changed_confirm.jsp");
		dispatcher.forward(request, response);

	}

}
