package mywebsite;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserSellChangedCon
 */
@WebServlet("/UserSellChangedCon")
public class UserSellChangedCon extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSellChangedCon() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");


		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);


		//jspに入力されたものをゲットして、変数に入れていく
		int itemCreaterId = Integer.parseInt(request.getParameter("hidden0"));
		String itemName = request.getParameter("hidden1");
		String itemDetail = request.getParameter("hidden2");

		//int型に直す
		int itemPrice = Integer.parseInt(request.getParameter("hidden3"));

		System.out.println(request.getParameter("hidden4"));
		int itemCatenum = Integer.parseInt(request.getParameter("hidden4"));

		int itemNumber = Integer.parseInt(request.getParameter("hidden8"));

		System.out.println(request.getParameter("hidden5"));

		String itemPic1 = request.getParameter("hidden5");
		String itemPic2 = request.getParameter("hidden6");
		String itemPic3 = request.getParameter("hidden7");

		String itemCate = request.getParameter("cate");

		//上記の情報をインスタンスに情報を入れていく
		ItemBeans item = new ItemBeans();

		item.setCreater_id(itemCreaterId);
		item.setItem_name(itemName);
		item.setDetail(itemDetail);
		item.setPrice(itemPrice);
		item.setCategory_id(itemCatenum);
		item.setNumber(itemNumber);
		item.setFile_name1(itemPic1);
		item.setFile_name2(itemPic2);
		item.setFile_name3(itemPic3);

		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);
		request.setAttribute("itemCate", itemCate);
		request.setAttribute("item", item);

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user_sell_changed.jsp");
		dispatcher.forward(request, response);

	}

}
