package mywebsite;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.CategoryBeans;
import beans.ItemBeans;
import beans.UserBeans;
import dao.CategoryDao;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class UserSellHistory
 */
@WebServlet("/UserSellHistory")
public class UserSellHistory extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserSellHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//セッションがあったらセットする
		HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");

		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");


		//idでユーザー情報を引き出す
		UserBeans user = UserDao.getUserInfoById(id);


		//リクエストスコープに上のメソッドをセット
		request.setAttribute("user", user);


		try {
			//販売履歴の情報を入手していく
			List<ItemBeans> list = ItemDao.getSellingItemByCreaterID(id);


			//購入した履歴がないときの処理
			if (list.size() == 0) {
				request.setAttribute("InfoMessage", "販売履歴がありません");
			}

			//listのかてごりーIdをもとにカテゴリーネームをリストに入れていく
			for (int i = 0; i < list.size(); i++) {

				CategoryBeans Cate = CategoryDao.getCategoryById(list.get(i).getCategory_id());
				String Categoryname = Cate.getCategory_name();
				list.get(i).setCategoryName(Categoryname);
			}

			//リクエストスコープに上のメソッドをセット
			request.setAttribute("list", list);

			//htmlでつっくった画面を出す
			// フォワード　サーブレットからJSPに行くitem_serch_result
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user-sell-history.jsp");
			dispatcher.forward(request, response);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

	}

}
