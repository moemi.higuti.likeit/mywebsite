CREATE DATABASE IF NOT EXISTS `mywebsite` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `mywebsite`;

CREATE TABLE `m_delivery_method` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- テーブルのデータのダンプ `m_delivery_method`
--

INSERT INTO `m_delivery_method` (`id`, `name`, `price`) VALUES
(1, '特急配送', 500),
(2, '通常配送', 0),
(3, '日時指定配送', 200);

-- --------------------------------------------------------

--
-- テーブルの構造 `t-item`
--

CREATE TABLE `t-item` (
  `id` int(11) NOT NULL,
  `creater_id` int(11) NOT NULL,
  `item_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `file_name1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name3` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;





--
-- テーブルの構造 `t_category`
--
CREATE TABLE `t_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
--  `t_category` 内容を入れる
--

DELETE FROM `t_category`;
INSERT INTO `t_category`(`id`, `category_name`,`category_img` ) VALUES
(1, '陶器', 'touki4.jpg'),
(2, '折り紙','origami.jpg'),
(3, '手芸','doll.jpg'),
(4, '手作りマスク','mask.jpg'),
(5, 'アクセサリー','akuse2.jpg'),
(6, 'その他','eggs.jpg');




--
-- テーブルの構造 `t-buy`
--
CREATE TABLE `t_buy` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `delivery_method_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


--
-- テーブルの構造 `t_buy_detail`

--買ったものの情報

CREATE TABLE `t_buy_detail` (
  `id` int(11) NOT NULL,
  `buy_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `category_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `t_buy_detail` DROP COLUMN `category_id`;
ALTER TABLE `t_buy_detail` ADD `category_id` int(11) NOT NULL;


--
-- テーブルの構造 `t_user`
--

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address1` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address2` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_password` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_icon` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;






--
-- Indexes for table `t_user`
--
ALTER TABLE `t-item`
  ADD PRIMARY KEY (`id`);


--
-- Indexes for table `m_delivery_method`
--
ALTER TABLE `m_delivery_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_item`
--
ALTER TABLE `t_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_buy`
--
ALTER TABLE `t_buy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_buy_detail`
--
ALTER TABLE `t_buy_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_sell_detail`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);


/*--------------------------------------------karamuwo 
hutasu ------------------------------------*/

ALTER TABLE `t_user`
  ADD `first_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL;

/* ーーーーーーーーーーーーーーーーー実行ーーーーーーーーーーーーーーーーーー*/

--
-- AUTO_INCREMENT for table `m_delivery_method`
--
ALTER TABLE `m_delivery_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_item`
--
ALTER TABLE `t-item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=301;
--
-- AUTO_INCREMENT for table `t_buy`
--
ALTER TABLE `t_buy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_buy_detail`
--
ALTER TABLE `t_buy_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;




/* ----------------       情報を入れる ---------------  */

USE `mywebsite`;



/*-----------------------アイテムを入れる----------------------------*/

INSERT INTO `t-item` (`id`, `creater_id`, `item_name`, `detail`, `price`, `category_id`, `file_name1`, `file_name2`, `file_name3`, `number`) VALUES
(2, 1, 'ペンギン','とにかくなんかすごくいい とにかくいい', 1500, 3, 'stuff.jpg','stuff.jpg','stuff.jpg',1),
                        (1, 1, 'すごい椅子','とにかくなんかすごくいい とにかくいい', 500, 6, 'eggs.jpg','eggs.jpg','eggs.jpg',10);
                        
INSERT INTO `t-item` (`id`, `creater_id`, `item_name`, `detail`, `price`, `category_id`, `file_name1`, `file_name2`, `file_name3`, `number`) VALUES
                      
                        (3, 1, '熊','とにかくなんかすごくいい とにかくいい', 2500, 3, 'stuff2.jpg','stuff2.jpg','stuff2.jpg',1),
                        (4, 1, '犬','とにかくなんかすごくいい とにかくいい', 1500, 3, 'stuff3.jpg','stuff3.jpg','stuff3.jpg',5),
                        (5, 2, 'カップ','とにかくいい', 500, 2, 'touki.jpg','touki.jpg','touki.jpg',10),
                        (6, 2, '陶器',' とにかくいい', 1500, 2, 'touki3.jpg','touki3.jpg','touki3.jpg',1),
                        (7, 2, 'コップ','とにかくいい', 2500, 2, 'touki2.jpg','touki2.jpg','touki2.jpg',1),
                        (8, 2, '皿','とにかくいい', 1500, 2, 'touki5.jpg','touki5.jpg','touki5.jpg',5),
                        (9, 3, 'origami',' とにかくいい', 1500, 3, 'origami.jpg','origami.jpg','origami.jpg',1),
                        (10, 3, '花','とにかくいい', 2500, 3, 'origami2.jpg','origami2.jpg','stuff2.jpg',1),
                        (11, 3, '立体的なやる','とにかくいい', 1500, 3, 'origami3.jpg','origami3.jpg','origami3.jpg',5),
                        (12, 3, '立体的','とにかくいい', 1500, 3, '805909_s.jpg','805909_s.jpg','805909_s.jpg',5),
                        (13, 4,'悪','とにかくいい', 2500, 5, 'aku.jpg','origami2.jpg','stuff2.jpg',1),
                        (14, 5, 'akuse','とにかくいい', 1500, 5, 'akuse.jpg','origami3.jpg','origami3.jpg',5),
                        (15, 5, 'akusesari-','とにかくいい', 1500, 5, 'akusesari-.jpg','origami3.jpg','origami3.jpg',5),
                        (16, 4, 'masuku-','とにかくいい', 1500, 4, 'akusesari-.jpg','origami3.jpg','origami3.jpg',5);
                        
                        
/*----------------トップページのランダムにおすすめ商品を入れるsql------------------------*/
                        
SELECT * FROM `t-item` ORDER BY RAND() LIMIT 4;

/*----------------トップページおすすめカテゴリーを入れるsql------------------------*/

SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id ;

SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id ORDER BY RAND() LIMIT 4;

SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id ORDER BY `t-item`.category_id, RAND() LIMIT 4; 

SELECT * FROM `t_category` JOIN `t-item` ON `t-item`.category_id = `t_category`.id ORDER BY `t-item`.category_id, RAND() LIMIT 4; 


/*---------------------　アイテム詳細に使う　カテゴリー名も欲しいのでジョイン　----------------------*/

SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id where `t-item`.id = 2;

/*---------------------カラムの追加----------------------*/

ALTER TABLE `t_category` ADD `category_img` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL;

/*ーーーーーーーーーーーーーあいまい検索して出たアイテムの数を数えるーーーーーーーーーーー*/

select count(*) as cnt from `t-item` where item_name like '%a%';



/*ーーーーーーーーーカテゴリーIdでカテゴリー名を取得ーーーーーーーーーーーーーーー*/

SELECT * FROM `t_category`where id = 0;


/*ーーーーーーーーーカテゴリー名からアイテムを出す取得ーーーーーーーーーーーーーーー*/

SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id where `t_category`.category_name ='折り紙'; ORDER BY `t-item`.id ASC LIMIT 1,8;


/*---------------カテゴリー名から　アイテムの数を数える-----------------------------*/

select count(*) as cnt from `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id  where `t_category`.category_name ='手芸';


/*---------------registar-----------------------------*/

INSERT INTO `t_user` ( `name`, `address1`, `address2`, `login_id`, `login_password`, `user_icon`,`create_date`,`first_name`) VALUES
( 'ペンギン', '南極','寒いところ','ペンさん' ,  'pooooooo', 'stuff.jpg',NOW(), '社長');
        

/*---------------idでユーザー情報を引き出す-----------------------------*/

SELECT * FROM `t_user` where id = 1;

SELECT * FROM `t_user` where login_id = 'PPPPPP';


/*----------------ログインIDとパスワードで探す----------------------------*/

SELECT * FROM `t_user` WHERE login_id = 'ペンさん' and login_password = 'p';


/*----------------ログインIDとパスワードで探す----------------------------*/

UPDATE `t_user` SET   name= ?, address1 = ?,address2=?, login_id= ?, login_password = ?,user_icon=?, first_name = ? WHERE id = ?;


UPDATE `t_user` SET  name='寒いところ', address1 ='寒いところ',address2='寒いところ', login_id= '寒いところ', login_password ='寒いところ', user_icon='touki3.jpg', first_name = 'tanakasan' WHERE id = 3;

update `t_user`  set 
UPDATE  表名
SET    列名 = 値 
WHERE　更新する行を特定する条件;'


/*----------------カテゴリー名でカテゴリーIdで探す----------------------------*/

select * from `t_category` where `category_name` = '陶器';


/*--------------   t-user.id To CREATERID を紐づける   ----------------------------*/

SELECT * FROM  `t_user` JOIN `t-item` ON `t-item`.creater_id = `t_user`.id where `t-item`.id = 1;



/* ----------------  上で取得したIDを入れて、商品を入れていく ---------------  */

INSERT INTO `t-item` ( `creater_id`, `item_name`, `detail`,     `price`, `category_id`, `file_name1`, `file_name2`, `file_name3`, `number`) VALUES
                       (1,            'ペンギン','とにかくいい', 1500,        3,        'stuff.jpg',   'stuff.jpg',   'stuff.jpg',  1);


/*----------------カテゴリー名でカテゴリーIdで探す----------------------------*/

select * from `t_category` where `id` = 1;



/*-------------- CREATERID　でUserID を紐づける   ----------------------------*/

SELECT * FROM  `t_user` JOIN `t-item` ON `t-item`.creater_id = `t_user`.id where `t-item`.creater_id = 1;


/*-------------- CREATERID　でUserID を紐づける   ----------------------------*/

SELECT * FROM  `m_delivery_method` ;



/*-------------- IDで配達の情報を取得 を紐づける   ----------------------------*/


SELECT * FROM  `m_delivery_method`where id = 1 ;



/*--------------  商品購入  ----------------------------*/


INSERT INTO t_buy(user_id,total_price,delivery_method_id,create_date) VALUES (1,1000,1,now());


/*--------------    ------商品の詳細の　挿入----------------------*/


INSERT INTO t_buy_detail(buy_id,item_id,category_id,number) VALUES (1,1,1,1);

/*--------------    ------商品の詳細の　挿入----------------------*/

delete from `t_user` where id =32;



/*--------------    ------商品の詳細の　挿入----------------------*/

delete from `t_user` where id =32;


/*--------------    ------ユーザーの購入情報を取得する----------------------*/

SELECT * FROM t_buy JOIN `m_delivery_method` ON t_buy.delivery_method_id = m_delivery_method.id WHERE t_buy.user_id = 33;



/*--------------    ------ユーザーの購入情報を取得する----------------------*/

SELECT * FROM `t-item`  WHERE creater_id = 29;


/*--------------    かてごりーId でカテゴリ名を取得----------------------------*/

select * from `t_category` where id = 1;


/*--------------    かてごりーId でカテゴリ名を取得----------------------------*/

ALTER TABLE `t-item`
  ADD `create_date` date DEFAULT NULL;
  
  
  
/*----------------商品入力--------------------------*/  
  INSERT INTO `t-item` ( `creater_id`, `item_name`, `detail`,`price`, `category_id`, `file_name1`, `file_name2`, `file_name3`, `number`,`create_date`) VALUES (1,1,1,1,1,1,1,1,1,now());




/*-------------- アイテムの情報を消す ----------------------------*/

delete from `t-item` where id =13;


/*--------------購入者のIdとそれに基づく情報を入手----------------------------*/

SELECT * FROM `t_buy`;

SELECT distinct `t_user`.id,`t_user`.login_id,`t_user`.user_icon  FROM `t_user` JOIN `t_buy` ON `t_buy`.user_id = `t_user`.id;


SELECT distinct `t_user`.id,  FROM `t_user` JOIN `t_buy` ON `t_buy`.user_id = `t_user`.id;



/*--------------販売者のIdとそれに基づく情報を入手----------------------------*/


SELECT distinct `t_user`.id,`t_user`.login_id,`t_user`.user_icon FROM `t-item` JOIN `t_user` ON `t-item`.creater_id = `t_user`.id;







/* ----------------       情報を入れる ---------------  */

USE `mywebsite`;

/* ---------------- アイテムの名前と残りの数を調べる ---------------  */


select `item_name` ,`number` from`t-item` ;


/* ---------------- アイテムの残りの数をアップデートしていく ---------------  */


UPDATE `t-item` SET `number` = 10-1 WHERE id = 1;



/* ---------------- アイテムの残りの数をアップデートしていく ---------------  */


SELECT * FROM `t-item`  where `number` not in (0);

SELECT * FROM `t-item`  where `number` = 0;


SELECT * FROM `t-item` where `number` not in (0) ORDER BY RAND() LIMIT 4 ;


SELECT * FROM `t-item` where `number` not in (0) ORDER BY id ASC LIMIT 0,8;

SELECT * FROM `t-item` WHERE item_name like '%い%' and `number` not in (0) ORDER BY id ASC LIMIT 0,8;

select count(*) as cnt from `t-item` where item_name like '%い%' and `number` not in (0);

select count(*) as cnt from `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id  where `t_category`.category_name ='手芸' and `number` not in (0);
 

SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id where `t_category`.category_name = '手芸' and `number` not in (0) ORDER BY `t-item`.id ASC LIMIT 0,8;


select count(*) as cnt from `t-item` where `number` = 0;

SELECT * FROM `t_buy` JOIN `m_delivery_method`  ON t_buy.delivery_method_id = m_delivery_method.id WHERE t_buy.id = 16;


SELECT * FROM `t_buy_detail` JOIN `t-item` ON `t_buy_detail`.item_id = `t-item`.id JOIN t_buy ON t_buy_detail.buy_id = t_buy.id  where buy_id = 1 ORDER BY price ASC;


SELECT * FROM `t-item` JOIN `t_category` ON `t-item`.category_id = `t_category`.id where `t-item`.id = 24;

/* ---------------スコアの評価 テーブル---------------  */


CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL, 
  `score` int(11)NOT NULL,
  `coment` text COLLATE utf8_unicode_ci,
  `create_date` date DEFAULT NULL 
) 

ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

select  DISTINCT  `t-item`.id,`t-item`.creater_id,`t-item`.item_name,
`t-item`.detail,`t-item`.price,`t-item`.category_id,`t-item`.file_name1,
`t-item`.file_name2,`t-item`.file_name3,`t-item`.number,`t-item`.create_date
 from `t-item` 
JOIN rating  ON `t-item`.id = `rating`.item_id where score = 4;

select  * 
 from `t-item` 
JOIN rating  ON `t-item`.id = `rating`.item_id where score = 4;

select  DISTINCT  `t-item`.id from `t-item` 
JOIN rating  ON `t-item`.id = `rating`.item_id where score = 4  ORDER BY `rating`.item_id;


select count(*) as cnt from `t-item` JOIN rating ON `t-item`.id = `rating`.item_id  where `rating`.score =4;
